<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!function_exists('autoriser')) {
	include_spip('inc/autoriser');
}     // si on utilise le formulaire dans le public

/**
 * Charger le formulaire : le formulaire liste les pages compatibles avec la noisette passée en argument et pouvant donc
 * recevoir cette même noisette dans le bloc fourni en argument.
 * La fonction déclare les champs postés et y intègre les valeurs par défaut.
 *
 * @param array  $noisette Tableau descriptif d'une noisette contenant à minima son type et son id.
 * @param string $id_page  Identifiant de la page
 * @param string $bloc     Bloc de page au sens Z.
 * @param string $redirect URL de redirection : on revient sur la page d'origine de l'action.
 *
 * @return array Tableau des champs postés pour l'affichage du formulaire.
 */
function formulaires_etendre_noisette_charger_dist(array $noisette, string $id_page, string $bloc, string $redirect) : array {
	// Initialisation
	$valeurs = [];

	// On récupère le type de page et la composition associé au type de noisette.
	// On récupère aussi sa nature conteneur ou pas qui s'applique donc à la noisette à copier.
	include_spip('inc/ncore_type_noisette');
	$type_noisette = type_noisette_lire('noizetier', $noisette['type_noisette']);
	$valeurs['est_conteneur'] = $type_noisette['conteneur'];

	// On acquiert le mode d'encapsulation auto : si auto, on ne présente pas la coche de copie de l'encappsulation
	// car on encapsule toujours. De fait, on considère que l'on copie le champ.
	include_spip('ncore_fonctions');
	$valeurs['encapsulation_auto'] = _NCORE_ENCAPSULATION_AUTO;

	// On cherche la liste des pages:
	// - compatibles avec la noisette
	// - dont le bloc concerné est bien configurable
	// - pouvant être configurées par l'utilisateur
	include_spip('inc/noizetier_page');
	$informations = ['nom', 'blocs_exclus'];
	$filtres = [];
	if (!empty($type_noisette['type'])) {
		$filtres['type'] = $type_noisette['type'];
	}
	if (!empty($type_noisette['composition'])) {
		$filtres['composition'] = $type_noisette['composition'];
	}
	$pages = page_noizetier_repertorier($informations, $filtres);

	$valeurs['_pages'] = [];
	if ($pages) {
		foreach ($pages as $_id_page => $_page) {
			if (($_id_page !== $id_page)
			and (!in_array($bloc, unserialize($_page['blocs_exclus'])))
			and autoriser('configurerpage', 'noizetier', null, null, ['page' => $_id_page])) {
				$valeurs['_pages'][$_id_page] = typo($_page['nom']) . " (<em>{$_id_page}</em>)";
			}
		}
	}

	// On désactive le formulaire si aucune page n'est disponible pour la copie et on envoie un message d'erreur.
	$valeurs['editable'] = true;
	if (!$valeurs['_pages']) {
		$valeurs['message_erreur'] = _T('noizetier:erreur_etendre_aucune_page', ['page' => $id_page]);
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

/**
 * Vérifier les saisies : on doit toujours choisir une page.
 *
 * @param array  $noisette Tableau descriptif d'une noisette contenant à minima son type et son id.
 * @param string $id_page  Identifiant de la page
 * @param string $bloc     Bloc de page au sens Z.
 * @param string $redirect URL de redirection : on revient sur la page d'origine de l'action.
 *
 * @return array Tableau des erreurs ou vide si aucune erreur.
 */
function formulaires_etendre_noisette_verifier_dist(array $noisette, string $id_page, string $bloc, string $redirect) : array {
	$erreurs = [];
	if (!_request('pages')) {
		$erreurs['pages'] = _T('info_obligatoire');
	}

	return $erreurs;
}

/**
 * Exécuter les traitements : dupliquer la noisette dans les pages choisies.
 *
 * @param array  $noisette Tableau descriptif d'une noisette contenant à minima son type et son id.
 * @param string $id_page  Identifiant de la page
 * @param string $bloc     Bloc de page au sens Z.
 * @param string $redirect URL de redirection : on revient sur la page d'origine de l'action.
 *
 * @return array Tableau des messages de bon traitement ou d'erreur.
 */
function formulaires_etendre_noisette_traiter_dist(array $noisette, string $id_page, string $bloc, string $redirect) : array {
	$retour = [];

	// Récupération des pages sélectionnées.
	$pages = _request('pages');

	// Construire la liste des paramètres à diffuser dans les noisettes copiées.
	// -- soit tout soit rien.
	$parametrage = _request('copie_parametres') ? null : [];

	// Pour chaque page on copie la noisette avec tous ses paramètres.
	// Il est inutile de tester l'autorisation sur la page car cela a déjà été fait lors du chargement.
	include_spip('inc/ncore_noisette');
	$erreurs = [];
	$conteneur = [];
	foreach ($pages as $_page) {
		// Définir le conteneur de la noisette, à savoir, le squelette du bloc de la page concernée.
		$conteneur['squelette'] = "{$bloc}/{$_page}";
		if (!noisette_dupliquer('noizetier', $noisette['id_noisette'], $conteneur, 0, $parametrage)) {
			$erreurs[] = $_page;
		}
	}

	if (!$erreurs) {
		$retour['message_ok'] = _T('info_modification_enregistree');
		$retour['redirect'] = $redirect;
	} else {
		$retour['message_erreur'] =
			_T('noizetier:erreur_etendre_noisette', ['pages', implode(', ', $erreurs)]);
	}

	return $retour;
}

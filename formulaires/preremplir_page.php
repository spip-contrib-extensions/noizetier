<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!function_exists('autoriser')) {
	include_spip('inc/autoriser');
}     // si on utilise le formulaire dans le public

/**
 * Charger le formulaire : le formulaire liste les pages compatibles permettant de pré-remplir la page ou le contenu vide en cours de
 * configuration. Les pages compatibles sont celles dont le type est le même que celui de la page ou du contenu.
 *
 * @param array|string $page_ou_objet Page au sens SPIP ou objet spécifiquement identifié.
 *                                    - dans le cas d'une page SPIP comme sommaire ou article, l'argument est une chaîne, l'identifiant de la page.
 *                                    - dans le cas d'un objet SPIP comme un article d'id x, l'argument est un tableau associatif à deux index,
 *                                    `objet` et `id_objet`.
 *
 * @return array Tableau des champs postés pour l'affichage du formulaire.
 */
function formulaires_preremplir_page_charger_dist($page_ou_objet) : array {
	// Initialisation
	$valeurs = [];
	$erreur = '';

	// On recherche toujours la liste des pages dont le type coincide avec la page ou le contenu à remplir.
	include_spip('inc/noizetier_page');
	$informations = 'nom';
	$filtres['type'] = is_array($page_ou_objet)
		? $page_ou_objet['objet']
		: page_noizetier_extraire_type($page_ou_objet);
	$pages = page_noizetier_repertorier($informations, $filtres);

	// On supprime les pages sans noisettes configurées qui ne servent à rien et on passe typo() sur le nom de celles
	// que l'on retient.
	foreach ($pages as $_id_page => $_nom_page) {
		if (array_sum(page_noizetier_compter_noisettes($_id_page)) === 0) {
			unset($pages[$_id_page]);
		} else {
			$pages[$_id_page] = typo($_nom_page);
		}
	}

	// On renvoie la liste mais pas d'erreur si la liste est vide
	$valeurs['_pages'] = $pages;

	// On désactive le formulaire si aucune page n'est disponible et on envoie un message d'erreur.
	$valeurs['editable'] = true;
	if ($erreur) {
		$valeurs['message_erreur'] = $erreur;
		$valeurs['editable'] = false;
	}

	return $valeurs;
}

/**
 * Vérifier les saisies : on doit toujours choisir une page source.
 *
 * @param array|string $page_ou_objet Page au sens SPIP ou objet spécifiquement identifié.
 *                                    - dans le cas d'une page SPIP comme sommaire ou article, l'argument est une chaîne, l'identifiant de la page.
 *                                    - dans le cas d'un objet SPIP comme un article d'id x, l'argument est un tableau associatif à deux index,
 *                                    `objet` et `id_objet`.
 *
 * @return array Tableau des erreurs ou vide si aucune erreur.
 */
function formulaires_preremplir_page_verifier_dist($page_ou_objet) : array {
	$erreurs = [];
	if (!_request('page_source')) {
		$erreurs['page_source'] = _T('info_obligatoire');
	}

	return $erreurs;
}

/**
 * Exécuter les traitements : dupliquer les noisettes de la page source choisie vers la page destination.
 *
 * @param array|string $page_ou_objet Page au sens SPIP ou objet spécifiquement identifié.
 *                                    - dans le cas d'une page SPIP comme sommaire ou article, l'argument est une chaîne, l'identifiant de la page.
 *                                    - dans le cas d'un objet SPIP comme un article d'id x, l'argument est un tableau associatif à deux index,
 *                                    `objet` et `id_objet`.
 *
 * @return array Tableau des messages de bon traitement ou d'erreur.
 */
function formulaires_preremplir_page_traiter_dist($page_ou_objet) : array {
	$retour = [];

	// Récupération des pages sélectionnées.
	$page_source = _request('page_source');

	// Récupération des noisettes de la page source de profondeur 0, classées par bloc puis par rang.
	// En effet, la fonction de duplication gérera la récursivité (profondeur > 0) si besoin.
	include_spip('inc/noizetier_page');
	$select = ['id_noisette', 'rang_noisette', 'bloc'];
	$from = 'spip_noisettes';
	$where = [
		'plugin=' . sql_quote('noizetier'),
		'profondeur=0',
		'type=' . sql_quote(page_noizetier_extraire_type($page_source)),
		'composition=' . sql_quote(page_noizetier_extraire_composition($page_source))
	];
	$order_by = ['bloc', 'rang_noisette'];
	$noisettes_source = sql_allfetsel($select, $from, $where, '', $order_by);

	// Injection des noisettes de la source dans la page.
	if ($noisettes_source) {
		// On détermine les blocs exclus de la page ou du contenu à remplir car ceux-ci ne recevront aucune noisette.
		// Un contenu donné se cale sur les blocs de son type de page, il ne possède pas une liste propre.
		$blocs_exclus = is_array($page_ou_objet)
			? page_noizetier_lire($page_ou_objet['objet'], 'blocs_exclus')
			: page_noizetier_lire($page_ou_objet, 'blocs_exclus');

		include_spip('inc/ncore_noisette');
		include_spip('inc/noizetier_conteneur');
		foreach ($noisettes_source as $_noisette) {
			// On ne copie pas les noisettes source incluses dans un bloc non autorisé par la
			// composition virtuelle créée.
			if (!$blocs_exclus or !in_array($_noisette['bloc'], $blocs_exclus)) {
				// On calcule le nouveau conteneur en fonction de la page ou de l'objet à remplir.
				$id_conteneur_destination = conteneur_noizetier_composer($page_ou_objet, $_noisette['bloc']);

				// On duplique la noisette source dans le conteneur de la composition virtuelle : le
				// rang de la noisette source est conservé et le paramétrage entièrement copié
				noisette_dupliquer(
					'noizetier',
					$_noisette['id_noisette'],
					$id_conteneur_destination,
					$_noisette['rang_noisette']
				);
			}
		}
		// On invalide le cache de la page ou de l'objet dans lequel est inclus le conteneur.
		include_spip('inc/invalideur');
		$invalideur = (!empty($page_ou_objet['objet']) and !empty($page_ou_objet['id_objet']))
			? "id='{$page_ou_objet['objet']}/{$page_ou_objet['id_objet']}'"
			: (!empty($page_ou_objet['page'])
				? "id='page/{$page_ou_objet['page']}'"
				: "id='page'");
		suivre_invalideur($invalideur);
	}

	return $retour;
}

<?php
/**
 * Ce fichier contient les filtres et balises du noiZetier.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// --------------------------------------------------------------------------------
// --------------------- API TYPES DE NOISETTE : COMPLEMENT -----------------------
// --------------------------------------------------------------------------------

/**
 * Surcharge du critère natif categories correspondant au champ homonyme de la table spip_types_noisettes.
 * Si le paramètre du critère est une liste de catégories, le critère applique un OU sur chaque catégorie
 * ou un ET si l'opérateur de négation est utilisé sur le critère.
 * Par contre; il est possible de combiner plusieurs critères `categories` dans une même boucle dont les
 * conditions sont combinées en ET.
 *
 * @package SPIP\NOIZETIER\TYPE_NOISETTE\CRITERE
 *
 * @uses calculer_critere_categories()
 *
 * @critere
 *
 * @example
 *   Utilisation standard avec un paramètre non vide:
 *   {type_noisette_categorie param} où param peut prendre les valeurs suivantes:
 *   - une valeur immédiate ou une liste de valeurs immédiates comme "c1,c2"
 *   - une variable d'un ENV ou GET qui elle-même peut être une chaine unique, une chaine de type liste séparée par
 *     des virgules ou un tableau
 *   - une combinaison comme c1,#GET{variable}
 *   {!type_noisette_categorie param} la négation de tous les cas précédents
 *   Cas d'un paramètre vide:
 *   {type_noisette_categorie} n'applique aucun critère
 *   {!type_noisette_categorie} renvoie les types de noisette sans catégorie (à priori n'existe pas)
 *
 * @param string  $idb     Identifiant de la boucle.
 * @param array   $boucles AST du squelette.
 * @param Critere $critere Paramètres du critère dans la boucle.
 *
 * @return void
 */
function critere_TYPES_NOISETTES_categories_dist(string $idb, array &$boucles, Critere $critere) : void {
	// Initialisation de la table (spip_mots ou spip_groupes_mots) et de la boucle concernée.
	$boucle = &$boucles[$idb];

	// Initialisation du numéro de critère pour utiliser plusieurs fois le critère dans la même boucle
	static $i = 1;

	// La fonction INSTR retourne soit 0 (pas trouvée) soit une valeur strictement supérieure à 0 (trouvée).
	// Donc avec le modificateur not l'opérateur est un "=", sinon un ">".
	// Le NOT s'utilise uniquement avec une branche SPIP (ex 2.0).
	$op = ($critere->not == '!') ? '=' : '>';

	// On calcule le code des critères.
	// -- Initialisation avec le chargement de la fonction de calcul du critère.
	$boucle->hash .= '
	// CATEGORIES TYPE_NOISETTE
	$conditionner = \'calculer_critere_categories\';';

	// On identifie les typologies explicitement fournies dans le critère.
	$categories = [];
	if (!empty($critere->param)) {
		// La ou les catégories sont explicites dans l'appel du critere.
		// - on boucle sur les paramètres sachant qu'il est possible de fournir une liste séparée par une virgule
		//   (ex c1, c2)
		foreach ($critere->param as $_param) {
			if (isset($_param[0])) {
				$categories[] = calculer_liste([$_param[0]], [], $boucles, $boucle->id_parent);
			}
		}
	}

	// On construit la condition en la calculant à l'exécution.
	$boucle->hash .= '
	$where' . $i . ' = $conditionner(array(' . implode(',', $categories) . '), \'' . $op . '\');';
	$boucle->where[] = '$where' . $i;
	$i++;
}

/**
 * Renvoie la condition SQL correspondant à l'utilisation du critère `categories`.
 *
 * @api
 *
 * @param array  $parametres Liste des identifiants de catégories. Si plusieurs catégories sont fournies la fonction
 *                           fait un OR sur les conditions unitaires.
 * @param string $operateur  Opérateur de comparaison, tel que '>' ou '='.
 *
 * @return string
 */
function calculer_critere_categories(array $parametres, string $operateur) : string {
	// Initialisation de la condition et de l'opérateur de conditions multiples
	if ($operateur === '>') {
		// Par défaut il faut annuler le critère, sinon l'opérateur de conditions multiples est un OU
		$condition = '1=1';
		$operateur_condition = 'OR';
	} else {
		$condition = 'categories=\'\'';
		$operateur_condition = 'AND';
	}

	// Construire la liste des conditions pour chaque catégorie.
	if ($parametres) {
		// Traiter le cas où la liste des catégories est fournie par un GET ou ENV composé d'un array ou d'une liste
		// -- il faut reconstruire une liste plate
		$categories = [];
		foreach ($parametres as $_parametre) {
			if (is_array($_parametre)) {
				// Le paramètre est passé par un GET ou ENV sous la forme d'un tableau
				$categories = array_merge($categories, $_parametre);
			} elseif (is_string($_parametre) and (strpos($_parametre, ',') !== false)) {
				// Le paramètre est passé par un GET ou ENV sous la forme d'une liste séparée par une virgule
				$categories = array_merge($categories, explode(',', $_parametre));
			} else {
				$categories[] = $_parametre;
			}
		}

		// Etant donné le format du champ ",id1,id2," on utilise un INSTR compatible mySQL et SQLite.
		// -- pour un critère, une liste est interprétée comme un OU.
		$condition = '';
		foreach ($categories as $_categorie) {
			if ($_categorie) {
				$condition .= ($condition ? " {$operateur_condition} " : '') . 'INSTR(categories, \',' . $_categorie . ',\') ' . $operateur . ' 0';
			}
		}
	}

	return $condition;
}

// --------------------------------------------------------------------------------
// ------------------------- API NOISETTES : COMPLEMENT ---------------------------
// --------------------------------------------------------------------------------

// --------------------------------------------------------------------------------
// ------------------------- API CONTENEURS : COMPLEMENT --------------------------
// --------------------------------------------------------------------------------

/**
 * Compile la balise `#CONTENEUR_NOIZETIER_IDENTIFIER` qui fournit l'id du conteneur désigné par ses éléments
 * canoniques propres au noiZetier. La balise est une encapsulation de la fonction `conteneur_noizetier_composer`
 * mais ne permet de calculer l'id d'un conteneur noisette car ce cas n'est pas utilisable dans un HTML. Elle est
 * à utiliser de préférence à celle fournie par N-Core (`#CONTENEUR_IDENTIFIER`).
 *
 * La signature de la balise est : `#CONTENEUR_NOIZETIER_IDENTIFIER{page_ou_objet, bloc}`.
 *
 * @package SPIP\NOIZETIER\CONTENEUR\BALISE
 *
 * @balise
 *
 * @example
 *     ```
 *     #CONTENEUR_NOIZETIER_IDENTIFIER{article, content}, renvoie l'id du conteneur représentant le bloc content/article
 *     #CONTENEUR_NOIZETIER_IDENTIFIER{array(objet => article, id_article => 12), content}, renvoie l'id du conteneur
 *     représentant le bloc content de l'objet article12
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 */
function balise_CONTENEUR_NOIZETIER_IDENTIFIER_dist(Champ $p) : Champ {
	$page_ou_objet = interprete_argument_balise(1, $p);
	$page_ou_objet = str_replace('\'', '"', $page_ou_objet);

	$bloc = interprete_argument_balise(2, $p);
	$bloc = str_replace('\'', '"', $bloc);

	$p->code = "calculer_id_conteneur({$page_ou_objet}, {$bloc})";

	return $p;
}

/**
 * Encapsule la fonction de composition d'un identifiant de conteneur.
 *
 * @internal
 *
 * @param array|string $page_ou_objet Page au sens SPIP ou objet spécifiquement identifié.
 *                                    - dans le cas d'une page SPIP comme sommaire, l'argument est une chaîne.
 *                                    - dans le cas d'un objet SPIP comme un article d'id x, l'argument est un tableau associatif à deux index,
 *                                    `objet` et `id_objet`.
 * @param string       $bloc          Bloc de page au sens Z.
 *
 * @return string Identifiant unique du conteneur au format chaine.
 */
function calculer_id_conteneur($page_ou_objet, string $bloc) : string {
	include_spip('inc/noizetier_conteneur');

	return conteneur_noizetier_composer($page_ou_objet, $bloc);
}

// -------------------------------------------------------------------
// --------------------------- API ICONES ----------------------------
// -------------------------------------------------------------------

/**
 * Compile la balise `#ICONE_NOIZETIER_LISTE` qui fournit la liste des icones d'une taille donnée en pixels
 * disponibles dans les thèmes SPIP de l'espace privé.
 * La signature de la balise est : `#ICONE_NOIZETIER_LISTE{taille}`.
 *
 * @package SPIP\NOIZETIER\ICONE\BALISE
 *
 * @balise
 *
 * @example
 *     ```
 *     #ICONE_NOIZETIER_LISTE{24}, renvoie les icones de taille 24px présents dans les thèmes du privé
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_ICONE_NOIZETIER_LISTE_dist(Champ $p) : Champ {
	$taille = interprete_argument_balise(1, $p);
	$taille = str_replace('\'', '"', $taille);
	$p->code = "calculer_liste_icones({$taille})";

	return $p;
}

/**
 * Calcule la liste des icones disponibles pour une taille donnée.
 *
 * @internal
 *
 * @param int $taille Taille de l'icone.
 *
 * @return array
 */
function calculer_liste_icones(int $taille = 24) : array {
	static $icones = null;

	if (null === $icones) {
		$pattern = ".+-{$taille}[.](svg|png)$";
		$icones = find_all_in_path('prive/themes/spip/images/', $pattern);
	}

	return $icones;
}

// -------------------------------------------------------------------
// ---------------------------- API BLOCS ----------------------------
// -------------------------------------------------------------------

/**
 * Compile la balise `#BLOC_NOIZETIER_INFOS` qui fournit un champ ou tous les champs descriptifs d'un bloc Z
 * donné. Ces champs sont lus dans le fichier YAML du bloc si il existe.
 * La signature de la balise est : `#BLOC_NOIZETIER_INFOS{bloc, information}`.
 *
 * @package SPIP\NOIZETIER\BLOC\BALISE
 *
 * @balise
 *
 * @example
 *     ```
 *     #BLOC_NOIZETIER_INFOS{content}, renvoie tous les champs descriptifs du bloc content
 *     #BLOC_NOIZETIER_INFOS{content, nom}, renvoie le titre du bloc content
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_BLOC_NOIZETIER_INFOS_dist(Champ $p) : Champ {
	$bloc = interprete_argument_balise(1, $p);
	$bloc = str_replace('\'', '"', $bloc);
	$information = interprete_argument_balise(2, $p);
	$information = isset($information) ? str_replace('\'', '"', $information) : '""';
	$p->code = "calculer_infos_bloc({$bloc}, {$information})";

	return $p;
}

/**
 * Encapsule la fonction de lecture d'un bloc.
 *
 * @internal
 *
 * @param string      $bloc        Identifiant du bloc
 * @param null|string $information Information à renvoyer ou vide si toutes les informations du bloc sont demandées
 *
 * @return array|string Informations sur le bloc ou uniquement une information précise.
 */
function calculer_infos_bloc(string $bloc, ?string $information = '') {
	include_spip('inc/noizetier_bloc');

	return bloc_noizetier_lire($bloc, $information);
}

// -------------------------------------------------------------------
// ---------------------------- API PAGES ----------------------------
// -------------------------------------------------------------------

/**
 * Compile la balise `#PAGE_NOIZETIER_INFOS` qui fournit un champ ou tous les champs descriptifs d'une page
 * ou d'une composition donnée. Ces champs sont lus dans la table `spip_noizetier_pages`.
 * La signature de la balise est : `#PAGE_NOIZETIER_INFOS{page, information}`.
 *
 * La fonction peut aussi renvoyer d'autres informations calculées, à savoir :
 * - `est_disponible` qui indique si le fichier est dans le PATH (plugin fournisseur désactivé mais base toujours remplie).
 * - `est_modifiee` qui indique si la configuration du fichier YAML ou XML de la page a été modifiée ou pas.
 * - `compteurs_type_noisette` qui donne le nombre de types de noisettes disponibles pour la page ou la composition
 *    donnée en distinguant les types de noisettes communs à toutes les pages, les types de noisettes spécifiques à
 *    un type de page et les types de noisettes spécifiques à une composition.
 * - `compteurs_noisette` qui donne le nombre de noisettes incluses dans chaque bloc de la page.
 *
 * @package SPIP\NOIZETIER\PAGE\BALISE
 *
 * @balise
 *
 * @example
 *     ```
 *     #PAGE_NOIZETIER_INFOS{article}, renvoie tous les champs descriptifs de la page article
 *     #PAGE_NOIZETIER_INFOS{article, nom}, renvoie le titre de la page article
 *     #PAGE_NOIZETIER_INFOS{article-forum, nom}, renvoie le titre de la composition forum de la page article
 *     #PAGE_NOIZETIER_INFOS{article, est_disponible}, indique si le fichier HTML de la page est accessible via le PATH
 *     #PAGE_NOIZETIER_INFOS{article, est_modifiee}, indique si la configuration de la page article a été modifiée
 *     #PAGE_NOIZETIER_INFOS{article, compteurs_type_noisette}, fournit les compteurs de types de noisette compatibles
 *     #PAGE_NOIZETIER_INFOS{article, compteurs_noisette}, fournit les compteurs de noisettes incluses par bloc
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_PAGE_NOIZETIER_INFOS_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- seul l'argument information est optionnel.
	$page = interprete_argument_balise(1, $p);
	$page = str_replace('\'', '"', $page);
	$information = interprete_argument_balise(2, $p);
	$information = isset($information) ? str_replace('\'', '"', $information) : '""';

	// Calcul de la balise
	$p->code = "calculer_infos_page({$page}, {$information})";

	return $p;
}

/**
 * Renvoyer tout ou partie des informations sur une page.
 *
 * @internal
 *
 * @param string      $id_page     Identifiant de la page
 * @param null|string $information Information à renvoyer ou vide si toutes les informations de la page sont demandées
 *
 * @return mixed Informations de la page ou une information précise.
 */
function calculer_infos_page(string $id_page, ?string $information = '') {
	include_spip('inc/noizetier_page');
	if ($information == 'est_modifiee') {
		// Initialisation du retour
		$retour = true;

		// Détermination du répertoire par défaut
		$repertoire = page_noizetier_initialiser_dossier();

		// Récupération du md5 enregistré en base de données
		$from = 'spip_noizetier_pages';
		$where = ['page=' . sql_quote($id_page)];
		$md5_enregistre = sql_getfetsel('signature', $from, $where);

		if ($md5_enregistre) {
			// On recherche d'abord le fichier YAML et sinon le fichier XML pour la compatibilité ascendante.
			if (($fichier = find_in_path("{$repertoire}{$id_page}.yaml"))
			or ($fichier = find_in_path("{$repertoire}{$id_page}.xml"))) {
				$md5 = md5_file($fichier);
				if ($md5 == $md5_enregistre) {
					$retour = false;
				}
			}
		}
	} elseif ($information === 'compteurs_type_noisette') {
		// Initialisation des compteurs par bloc
		$retour = [
			'composition' => 0,
			'type'        => 0,
			'commun'      => 0
		];

		// Acquisition du type et de la composition éventuelle.
		$type = page_noizetier_extraire_type($id_page);
		$composition = page_noizetier_extraire_composition($id_page);

		// Les compteurs de types de noisette d'une page sont calculés par une lecture de la table 'spip_types_noisettes'.
		$from = ['spip_types_noisettes'];
		$where = [
			'plugin=' . sql_quote('noizetier'),
			'type=' . sql_quote($type),
			'composition=' . sql_quote($composition)
		];
		$compteur = sql_countsel($from, $where);

		// On cherche maintenant les 3 compteurs possibles :
		if ($composition) {
			// - les types de noisette spécifiques de la composition si la page en est une.
			if ($compteur) {
				$retour['composition'] = $compteur;
			}
			$where[2] = 'composition=' . sql_quote('');
			$compteur = sql_countsel($from, $where);
			if ($compteur) {
				$retour['type'] = $compteur;
			}
		} elseif ($compteur) {
			// - les types de noisette spécifiques de la page ou du type de la composition
			$retour['type'] = $compteur;
		}
		// - les types de noisette communs à toutes les pages.
		$where[1] = 'type=' . sql_quote('');
		$compteur = sql_countsel($from, $where);
		if ($compteur) {
			$retour['commun'] = $compteur;
		}

		$retour['total'] = array_sum($retour);
	} elseif ($information === 'compteurs_noisette') {
		$retour = page_noizetier_compter_noisettes($id_page);
	} elseif ($information === 'est_disponible') {
		// Si la page est virtuelle, elle est par définition toujours disponible. Sinon, il faut tester
		// l'existence du fichier HTML.
		$est_virtuelle = page_noizetier_lire($id_page, 'est_virtuelle');
		if ($est_virtuelle === 'oui') {
			$retour = true;
		} else {
			include_spip('inc/utils');
			$dossier_page = page_noizetier_initialiser_dossier();
			$retour = find_in_path("{$dossier_page}{$id_page}.html") !== false;
		}
	} else {
		$retour = page_noizetier_lire($id_page, $information, true);
	}

	return $retour;
}

// --------------------------------------------------------------------
// ---------------------------- API OBJETS ----------------------------
// --------------------------------------------------------------------

/**
 * Compile la balise `#OBJET_NOIZETIER_INFOS` qui fournit un champ ou tous les champs descriptifs d'un objet
 * donné. Ces champs sont lus dans la table de l'objet.
 * La signature de la balise est : `#OBJET_NOIZETIER_INFOS{type_objet, id_objet, information}`.
 *
 * La fonction peut aussi renvoyer d'autres informations calculées, à savoir :
 * - `compteurs_noisette` qui donne le nombre de noisettes incluses dans chaque bloc de l'objet.
 *
 * @package SPIP\NOIZETIER\OBJET\BALISE
 *
 * @balise
 *
 * @example
 *     ```
 *     #OBJET_NOIZETIER_INFOS{article, 12}, renvoie tous les champs descriptifs de l'article 12
 *     #OBJET_NOIZETIER_INFOS{article, 12, nom}, renvoie le titre de l'article 12
 *     #OBJET_NOIZETIER_INFOS{article, 12, compteurs_noisette}, fournit les compteurs de noisettes incluses par bloc pour l'article 12
 *     #OBJET_NOIZETIER_INFOS{article, 12, type_est_active}, renvoie si les noisettes sont autorisées sur l'article (tous les articles)
 *     #OBJET_NOIZETIER_INFOS{article, 12, composition_est_activee}, renvoie si les compositions sont activées sur l'article (tous les articles)
 *     #OBJET_NOIZETIER_INFOS{article, 12, composition_active}, reboie la composition active sur l'article 12
 *     ```
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_OBJET_NOIZETIER_INFOS_dist(Champ $p) : Champ {
	// Récupération des arguments de la balise.
	// -- seul l'argument information est optionnel.
	$objet = interprete_argument_balise(1, $p);
	$objet = str_replace('\'', '"', $objet);
	$id_objet = interprete_argument_balise(2, $p);
	$id_objet = isset($id_objet) ? $id_objet : '0';
	$information = interprete_argument_balise(3, $p);
	$information = isset($information) ? str_replace('\'', '"', $information) : '""';

	// Calcul de la balise
	$p->code = "calculer_infos_objet({$objet}, {$id_objet}, {$information})";

	return $p;
}

/**
 * Renvoie toutes les informations d'un objet ou seulement une information précise.
 *
 * @internal
 *
 * @param string      $type_objet  Le type d'objet comme `article`.
 * @param int         $id_objet    L'id de l'objet.
 * @param null|string $information Information à renvoyer ou vide si toutes les informations de l'objet sont demandées
 *
 * @return mixed Informations de l'objet ou une information précise.
 */
function calculer_infos_objet(string $type_objet, int $id_objet, ?string $information = '') {
	include_spip('inc/noizetier_objet');
	if ($information === 'compteurs_noisette') {
		$retour = objet_noizetier_compter_noisettes($type_objet, $id_objet);
	} elseif ($information === 'type_est_active') {
		$retour = objet_noizetier_type_active($type_objet);
	} elseif ($information === 'composition_active') {
		$retour = objet_noizetier_lire_composition($type_objet, $id_objet);
	} elseif ($information === 'composition_est_activee') {
		include_spip('inc/noizetier_page');
		$retour = page_noizetier_composition_activee($type_objet);
	} else {
		$retour = objet_noizetier_lire($type_objet, $id_objet, $information);
	}

	return $retour;
}

/**
 * Compile la balise `#OBJET_NOIZETIER_LISTE` qui renvoie la liste des objets possédant des noisettes
 * configurées. Chaque objet est fourni avec sa description complète.
 * La signature de la balise est : `#OBJET_NOIZETIER_LISTE`.
 *
 * @balise
 *
 * @param Champ $p Pile au niveau de la balise.
 *
 * @return Champ Pile complétée par le code à générer.
 **/
function balise_OBJET_NOIZETIER_LISTE_dist(Champ $p) : Champ {
	// Aucun argument à la balise.
	$p->code = 'calculer_liste_objets()';

	return $p;
}

/**
 * Renvoie la liste des objets ayant des noisettes.
 *
 * @internal
 *
 * @return array Liste des objets
 */
function calculer_liste_objets() : array {
	include_spip('inc/noizetier_objet');

	return objet_noizetier_repertorier();
}

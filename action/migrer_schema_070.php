<?php
/**
 * Ce fichier contient l'action `migrer_schema_070` lancée une seule fois par un utilisateur pour
 * migrer le contenu de la base du schéma 0.6.0 au schéma 0.7.0 de façon sécurisée.
 *
 * @package SPIP\NOIZETIER\TYPE_NOISETTE\ACTION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de migrer le contenu de la base du schéma 0.6.0 au schéma 0.7.0,
 * de façon sécurisée,
 * Les types de noisette sont rechargés pour peupler le champ css_saisies et les noisettes sont mises à jour
 * pour peupler le champ css_saisies et mettre à jour les champs paramètres et css des noisettes conteneur.
 *
 * Cette action est réservée aux utilisateurs pouvant utiliser le noiZetier.
 * Elle ne nécessite aucun argument.
 *
 * @return void
 */
function action_migrer_schema_070_dist() : void {
	// Sécurisation.
	// -- Aucun argument attendu.

	// Verification des autorisations : pour recharger les noisettes il suffit
	// d'avoir l'autorisation minimale d'accéder au noizetier.
	if (!autoriser('noizetier')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement des types de noisettes pour remplir correctement le nouveau champ css_saisies
	include_spip('inc/ncore_type_noisette');
	type_noisette_charger('noizetier', true);

	// Migration des noisettes existantes : calcul du nouveau champ css_saisies à partir du champ css existant.
	// Cette méthode n'est pas déterministe et suppose que le nombre de classes équivaut exactement au nombre valeurs
	// possibles pour l'ensemble des saisies.
	// -- récupérer les types de noisettes pour en extraire les saisies CSS venant d'être mises à jour
	$types_noisettes = type_noisette_repertorier('noizetier');

	// -- récupérer les noisettes existantes.
	include_spip('inc/ncore_noisette');
	$noisettes = noisette_repertorier('noizetier', [], 'id_noisette');

	// -- vérifier que tous les types de noisette nécessaires aux noisettes à migrer sont bien disponibles.
	//    si ce n'est pas le cas on ne migre pas les noisettes mais on affiche un message d'erreur : la migration pourra
	//    reprendre plus tard.
	$type_noisettes_indisponibles = array_diff(
		array_values(array_unique(array_column($noisettes, 'type_noisette'))),
		array_keys($types_noisettes)
	);
	if ($type_noisettes_indisponibles) {
		include_spip('inc/minipres');
		$titre = _T('noizetier:noizetier');
		$corps = _T(
			'noizetier:migration_070_erreur_type_noisette',
			['types' => implode(', ', $type_noisettes_indisponibles)]
		);
		$corps = generer_form_ecrire(
			'noizetier_pages',
			$corps,
			'',
			_T('noizetier:liste_pages')
		);
		echo minipres($titre, $corps);
		exit();
	}

	// -- mettre à jour chaque noisette
	include_spip('inc/ncore_noisette');
	foreach ($noisettes as $_id_noisette => $_noisette) {
		// Type de noisette, indicateur de conteneur et configuration des saisies CSS pour le type
		$type_noisette = $_noisette['type_noisette'];
		$est_conteneur = $_noisette['est_conteneur'] === 'oui';
		$saisies_css = unserialize($types_noisettes[$type_noisette]['css_saisies']);

		// Extraction de la valeur des CSS suivant que la noisette est un conteneur ou pas.
		// Si la noisette est un conteneur, il faut aussi modifier les champs paramètres et css.
		$modifications = [];
		$conteneur_css = '';
		if (!$est_conteneur) {
			$valeurs_css = explode(' ', trim($_noisette['css']));
		} else {
			$parametres = unserialize($_noisette['parametres']);
			if (isset($parametres['conteneur_css'])) {
				$conteneur_css = trim($parametres['conteneur_css']);
				unset($parametres['conteneur_css']);
			}
			$valeurs_css = explode(' ', $conteneur_css);
			// On met à jour les paramètres et la chaine des css
			$modifications['parametres'] = serialize($parametres);
			$modifications['css'] = $conteneur_css;
		}

		// Boucle sur les saisies CSS afin d'affecter une valeur à chaque
		$css = [];
		include_spip('inc/saisies');
		include_spip('inc/saisies_lister');
		foreach ($saisies_css as $_saisie) {
			if (($_saisie['options']['nom'] === 'css_saisies[ncore_defaut]')) {
				// On gère en priorité le cas le plus courant où le plugin noiZetier_extra n'a pas été utilisé
				// et que donc la saisie est unique et correspond à la saisie par défaut de N-Core.
				$css['ncore_defaut'] = !$est_conteneur ? $_noisette['css'] : $conteneur_css;
			} else {
				// Le migration se fait sur une site qui utilise noizetier_extra
				$type_saisie = $_saisie['saisie'];
				if (preg_match('#css_saisies\[(\w+)\]#', $_saisie['options']['nom'], $matches)) {
					$index_css = $matches[1];
					$css[$index_css] = '';
					$css_acceptes = [];
					if (include_spip("saisies/{$type_saisie}")) {
						$verifier_valeurs_acceptables = $type_saisie . '_valeurs_acceptables';
						if (function_exists($verifier_valeurs_acceptables)) {
							$_saisie['options']['nom'] = $index_css;
							foreach ($valeurs_css as $_css) {
								if ($verifier_valeurs_acceptables($_css, $_saisie)) {
									$css_acceptes[] = $_css;
								}
							}
						}
					}

					// Si on obtient un tableau à un élément c'est qu'on a une saisies simple
					if (count($css_acceptes) === 1) {
						$css[$index_css] = $css_acceptes[0];
					} elseif ($css_acceptes) {
						$css[$index_css] = $css_acceptes;
					}
				}
			}
		}

		// Mise à jour en base du champ css_saisies
		$modifications['css_saisies'] = serialize($css);
		noisette_parametrer('noizetier', $_id_noisette, $modifications);
	}

	// On désactive l'indicateur de migration à lancer
	include_spip('inc/config');
	ecrire_config('noizetier_migration', '');
}

<?php
/**
 * Ce fichier contient l'action `ajouter_noisette` lancée par un utilisateur autorisé pour ajouter une noisette
 * à un conteneur.
 *
 * @package SPIP\NOIZETIER\NOISETTE\ACTION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet d'ajouter de façon sécurisée une noisette à un conteneur.
 * Elle est utilisée via js sur le drag 'n drop.
 *
 * Cette action est réservée aux utilisateurs autorisés à configurer les pages du noiZetier.
 * Elle ne nécessite aucun arguments car ceux-ci sont récupérés via le formulaire.
 *
 * @uses conteneur_noizetier_decomposer()
 * @uses noisette_ajouter()
 *
 * @return void
 */
function action_ajouter_noisette_dist() : void {
	// Initialisation de la variable d'état de la fonction
	$retour = [
		'done'    => 'false',
		'success' => [],
		'errors'  => [],
	];

	// Récupération des inputs du formulaire d'ajout
	$type_noisette = _request('_type_noisette');
	$id_conteneur = _request('_id_conteneur');
	$rang = (int) (_request('rang'));

	// Décomposition de l'id du conteneur en éléments du noiZetier
	include_spip('inc/noizetier_conteneur');
	$conteneur = conteneur_noizetier_decomposer($id_conteneur);

	// Test de l'autorisation
	if (!autoriser('configurerpage', 'noizetier', null, null, $conteneur)) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Ajout de la noisette au conteneur choisi.
	include_spip('inc/ncore_noisette');
	$id_noisette = noisette_ajouter('noizetier', $type_noisette, $conteneur, $rang);

	// Préparation du tableau de retour pour l'envoi en JSON.
	if ((int) $id_noisette) {
		$retour['done'] = true;
		$retour['success'] = [$id_noisette];
	} else {
		$retour['errors'] = [_T('noizetier:erreur_ajout_noisette', ['noisettes' => $type_noisette])];
	}

	header('Content-Type: application/json; charset=' . $GLOBALS['meta']['charset']);
	echo json_encode($retour);
}

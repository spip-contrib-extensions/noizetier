<?php
/**
 * Ce fichier contient l'action `recharger_pages` lancée par un utilisateur pour
 * recharger le fichier de configuration de chaque page de façon sécurisée.
 *
 * @package SPIP\NOIZETIER\PAGE\ACTION
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cette action permet à l'utilisateur de recharger en base de données, de façon sécurisée,
 * une page ou l'ensemble des pages à partir de leur fichier XML ou YAML.
 *
 * Cette action est réservée aux utilisateurs pouvant utiliser le noiZetier.
 * Elle ne nécessite l'identifiant de la page ou vide si on veut recharger toutes les pages.
 *
 * @return void
 */
function action_recharger_pages_dist() : void {
	// Securisation et autorisation.
	// L'argument attendu est la page à recharger ou sinon vide pour toutes les pages.
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$page = $securiser_action();

	// Vérification des autorisations : pour recharger les pages ou les noisettes il suffit
	// d'avoir l'autorisation minimale d'accéder au noizetier.
	if (!autoriser('noizetier')) {
		include_spip('inc/minipres');
		echo minipres();
		exit();
	}

	// Rechargement des pages ou de la page : on force le recalcul complet, c'est le but.
	include_spip('inc/noizetier_page');
	page_noizetier_charger(true, $page);
}

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/noizetier?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// A
	'apercu' => 'Anketa',

	// B
	'bloc_sans_noisette' => 'V tomto bloku nie sú žiadne oriešky.', # MODIF
	'bouton_configurer_noisettes_composition' => 'Configurer pour cette composition', # MODIF
	'bouton_configurer_noisettes_objet' => 'Configurer pour ce contenu', # MODIF

	// C
	'compositions_non_installe' => '<b>Zásuvný modul Rozmiestnenia:</b> tento zásuvný modul nie je na vašej stránke nainštalovaný. Na prevádzku noiZetiera nie je potrebný. Keď je však aktivovaný, môžete definovať rozmiestnenia priamo do noiZetiera.',

	// D
	'description_bloc_contenu' => 'Hlavný text stránky.',
	'description_bloc_extra' => 'Ďalšie kontextové informácie pre ďalšiu stránku.',
	'description_bloc_navigation' => 'Informácie o navigácii na každej stránke.',
	'description_bloctexte' => 'Názov je nepovinný.Pri písaní textu môžete používať klávesové skratky SPIPu.',

	// E
	'editer_noizetier_explication' => 'Vyberte si stránku, na ktorej chcete nastaviť oriešky.',
	'editer_noizetier_titre' => 'Riadiť oriešky',
	'erreur_aucune_noisette_selectionnee' => 'Musíte si vybrať oriešok!',
	'explication_code' => 'POZOR! Pre pokročilých používateľov. Môžete zadať kód SPIPu (cykly a tagy), ktorý sa zobrazí tak, akoby to bola šablóna. Oriešok bude mať prístup aj k všetkým premenným prostredia danej stránky.',
	'explication_description_code' => 'Na interné použitie. Nezobrazí sa na verejne prístupnej stránke.',
	'explication_glisser_deposer' => 'Môžete pridať oriešok alebo oriešky inak usporiadať jednoduchým kliknutím a uvoľnením myši.', # MODIF
	'explication_heritages_composition' => 'Tu môžete nastaviť rozmiestnenia, ktoré budú používať objekty danej vetvy.', # MODIF

	// F
	'formulaire_ajouter_noisette' => 'Pridať oriešok',
	'formulaire_composition' => 'Identifikátor rozloženia',
	'formulaire_composition_explication' => 'Zadajte jedinečné kľúčové slovo (malými písmenami, bez medzier, bez pomlčiek (-) a diakritiky), ktoré umožní jednoznačne označiť toto rozmiestnenie.<br />Napríklad: <i>mojeroz.</i>', # MODIF
	'formulaire_deplacer_bas' => 'Posunúť nadol',
	'formulaire_deplacer_haut' => 'Posunúť nahor',
	'formulaire_description' => 'Opis',
	'formulaire_description_explication' => 'Môžete využívať zvyčajné skrtky SPIPu, najmä tag &lt;multi&gt;.', # MODIF
	'formulaire_erreur_format_identifiant' => 'V identifikátore môžu byť len malé písmená bez diakritiky, čísla a znak _ (podčiarkovník).',
	'formulaire_icon' => 'Ikona',
	'formulaire_icon_explication' => 'Môžete zadať relatívnu adresu umiestnenia ikony (napríklad: <i>images/objet-liste-contenus.png</i>).', # MODIF
	'formulaire_identifiant_deja_pris' => 'Tento identifikátor sa už používa!',
	'formulaire_modifier_composition' => 'Upraviť toto rozloženie:', # MODIF
	'formulaire_modifier_composition_heritages' => 'Upraviť závislosti tohto rozmiestnenia:', # MODIF
	'formulaire_modifier_noisette' => 'Upraviť tento oriešok',
	'formulaire_modifier_page' => 'Upraviť túto stránku', # MODIF
	'formulaire_nom' => 'Názov',
	'formulaire_nom_explication' => 'Môžete používať tag  &lt;multi&gt;.', # MODIF
	'formulaire_obligatoire' => 'Povinné polia',
	'formulaire_supprimer_noisette' => 'Odstrániť tento oriešok',
	'formulaire_supprimer_noisettes_page' => 'Odstrániť oriešky tejto stránky', # MODIF
	'formulaire_supprimer_page' => 'Odstrániť túto stránku', # MODIF
	'formulaire_type' => 'Typ stránky',

	// I
	'icone_introuvable' => 'Ikona sa nenašla!',
	'ieconfig_noizetier_export_explication' => 'Exportuje nastavenia orieškov a rozmiestnenia modulu noiZetier.', # MODIF
	'ieconfig_noizetier_export_option' => 'Zaradené do exportu?', # MODIF
	'ieconfig_non_installe' => '<b>Zásuvný modul Nahrávanie a export nastavení):</b> tento zásuvný modul nie je na vašej stránke nainštalovaný. Na spúšťanie noiZetiera nie je potrebný. Keď si ho však aktivujete, budete môcť exportovať a nahrávať nastavenia orieškov do noiZetiera.',
	'ieconfig_probleme_import_config' => 'Pri nahrávaní nastavení modulu noiZetier sa vyskytol problém.',

	// L
	'label_code' => 'Kód SPIPu:',
	'label_description_code' => 'Opis:',
	'label_niveau_titre' => 'Úroveň nadpisu:',
	'label_texte' => 'Text:',
	'label_titre' => 'Názov:',
	'liste_pages' => 'Zoznam stránok',

	// M
	'masquer' => 'Schovať',
	'mode_noisettes' => 'Upraviť oriešky',

	// N
	'ne_pas_definir_d_heritage' => 'Nedefinovať dedičnosť', # MODIF
	'noisette_numero' => 'oriešok číslo:',
	'noisettes_composition' => 'Oriešky, ktoré sa používa iba toto rozmiestnení <i>@composition@:</i>',
	'noisettes_disponibles' => 'Dostupné oriešky', # MODIF
	'noisettes_page' => 'Špeciálne oriešky pre stránku <i>@type@</i>:', # MODIF
	'noisettes_toutes_pages' => 'Oriešky spoločné pre všetky stránky:', # MODIF
	'noizetier' => 'noiZetier',
	'nom_bloc_contenu' => 'Obsah',
	'nom_bloc_extra' => 'Extra',
	'nom_bloc_navigation' => 'Navigácia',
	'nom_bloctexte' => 'Blok voľného textu',
	'nom_codespip' => 'Voľný kód SPIPu',

	// P
	'probleme_droits' => 'Na vykonanie tejto zmeny nemáte dostatočné práva.',

	// Q
	'quitter_mode_noisettes' => 'Opustiť úpravu orieškov',

	// R
	'retour' => 'Späť',

	// S
	'suggestions' => 'Podnety',
];

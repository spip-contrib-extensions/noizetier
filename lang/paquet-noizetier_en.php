<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-noizetier?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// N
	'noizetier_description' => 'The noiZetier provides an interface in the private area that lets you choose, for each page, blocks to be added to the content, navigation and extra zones of each page. The blocks thus complement the default content provided by the template.',
	'noizetier_nom' => 'Noizetier Page Builder',
	'noizetier_slogan' => 'Page Builder, natively functional with Z templates',
];

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/noizetier?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_configurer_noisettes_composition' => 'Configurer pour cette composition', # MODIF
	'bouton_configurer_noisettes_objet' => 'Configurer pour ce contenu', # MODIF

	// C
	'compositions_non_installe' => '<b>پلاگين تركيب‌ها: </b> اين پلاگين روي سايت شما نصب نشده است. براي كاركرد درست فندق‌ها لازم نيست. با اين همه، وقتي فعال شود، مي‌توانيد به صورت مستقيم در درون «مديريت فندق‌ها»ي خود تركيب‌هاي ايجاد كنيد. 
',

	// D
	'description_bloc_contenu' => 'محتواي اصلي هر صفحه.',
	'description_bloc_extra' => 'اطلاعات وراي متني براي هر صفحه.',
	'description_bloc_navigation' => 'اطلاعات ناوبري مناسب براي هر صفحه.',
	'description_bloctexte' => 'تيتر دلبخواهي است. براي متن، مي‌توانيد از ميان‌برهاي حروف‌نگاري اسپيپ استفاده كنيد.',

	// E
	'editer_noizetier_explication' => 'پيكربندي فندق‌ها براي افزودن به صفحه‌هاي سايت شما.', # MODIF
	'editer_noizetier_titre' => 'مديريت فندق‌ها',
	'explication_heritages_composition' => 'اينجا مي‌توانيد تركيب‌هايي را تنظيم كنيد كه اشياي اين شاخه به ارث خواهند برد.
', # MODIF

	// F
	'formulaire_ajouter_noisette' => 'افزودن يك فندق',
	'formulaire_composition' => 'شناسه تركيب ',
	'formulaire_composition_explication' => 'يك كليد واژه‌ي منحصر به فرد (بدون حروف كوچك، بدون فاصله، بدون خط تيره (-)، بدون آكسون) مشخص كنيد كه تا اين تركيب را شناسايي كند.  <br />به عنوان نمونه: <i>macompo</i>. ', # MODIF
	'formulaire_deplacer_bas' => 'حركت به پائين',
	'formulaire_deplacer_haut' => 'حركت به بالا',
	'formulaire_description' => 'توصيف ',
	'formulaire_description_explication' => 'مي‌توانيد از ميان‌برهاي معمولي اسپيپ، به ويژه برچسب &lt;multi&gt استفاده كنيد', # MODIF
	'formulaire_erreur_format_identifiant' => 'شناسه نمي‌تواند شامل چيزي جز حروف كوچ بدون آكسان و كارآكتر _ (زيزخط) باشد.',
	'formulaire_icon' => 'صورتك',
	'formulaire_icon_explication' => 'مي‌توانيد مسير نسبي به سوي يك صورتك را وارد كنيد (براي نمونه : <i>images/objet-liste-contenus.png</i>)
براي ديدن فهرست تصاوير اخيراً نصب شده مي‌توانيد  به اين صفحه مرجعه كنيد: <a href="../spip.php?page=icones_preview">', # MODIF
	'formulaire_identifiant_deja_pris' => 'از اين شناسه اكنون استفاده مي‌شود!',
	'formulaire_modifier_composition' => 'اصلاح اين تركيب: ', # MODIF
	'formulaire_modifier_composition_heritages' => 'اصلاح ميراث اين تركيب: ', # MODIF
	'formulaire_modifier_noisette' => 'اصلاح اين فندق',
	'formulaire_nom' => 'تيتر',
	'formulaire_nom_explication' => 'مي‌توانيد از برچسب &lt;multi&gt استفاده كنيد؛', # MODIF
	'formulaire_obligatoire' => 'ميدان الزامي ',
	'formulaire_supprimer_noisette' => 'حذف اين فندق',
	'formulaire_supprimer_noisettes_page' => 'حذف فندق‌هاي اين صفحه ', # MODIF
	'formulaire_type' => 'نوع تركيب ', # MODIF

	// I
	'ieconfig_noizetier_export_explication' => 'صادر سازي پيكربندي فندق‌ها و تركيب‌هاي فندق‌ها.', # MODIF
	'ieconfig_noizetier_export_option' => 'گنجاندن در صادرات؟', # MODIF
	'ieconfig_non_installe' => '<b>پلاگين صادرسازي/واردسازي پيكربندي‌ها: </b> اين پلاگين روي سايت شما نصب نشده. نيازي به پيكربندي فندق‌ها نيست. با اين همه، اگر فعال شود، مي‌توانيد پيكربندي‌هاي فندق‌هاي ذاخل فندق را صادر و وارد كنيد.',
	'ieconfig_probleme_import_config' => 'هنگام واردسازي پيكربندي فندق‌ها مشكلي رخ داده است.',

	// L
	'label_niveau_titre' => 'تراز تيتر: ',
	'label_texte' => 'متن:',
	'label_titre' => 'تيتر:',

	// N
	'ne_pas_definir_d_heritage' => 'تعريف نكردن ميراث', # MODIF
	'noisettes_composition' => 'فندق‌هاي مشخص شده در تركيب <i>@composition@</i>:',
	'noisettes_page' => 'فندق‌هاي مشخص شده براي صفحه‌ي <i>@type@</i> :', # MODIF
	'noisettes_toutes_pages' => 'فندق‌هاي مشترك در تمام صفحه‌ها: ', # MODIF
	'noizetier' => 'درخت فندق',
	'nom_bloc_contenu' => 'مطلب',
	'nom_bloc_extra' => 'فوق‌العاده ',
	'nom_bloc_navigation' => 'ناوبري',
	'nom_bloctexte' => 'بلوك متن آزاد',
];

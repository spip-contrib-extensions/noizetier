<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/noizetier?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// A
	'apercu' => 'Ver', # "Aperçu" lo he traducido como "ver", podría también traducirse como "visión de conjunto"

	// B
	'bloc_sans_noisette' => 'Este bloc no contiene noisette ', # "Bloc" en castellano sería "bloque", y "noisette" "nuez", pero prefiero dejar las palabras  en francés, pues son términos que considero es preferible dejarlos en la lengua de origen. MODIF
	'bouton_configurer_noisettes_composition' => 'Configurer pour cette composition', # MODIF
	'bouton_configurer_noisettes_objet' => 'Configurer pour ce contenu', # MODIF

	// C
	'compositions_non_installe' => '<b>Plugin compositions :</b> este  plugin no está instalado en vuestra Web. No es necesario para el funcionamiento del Noizetier. No obstante, si está activado, usted podrá declarar composiciones directamente en el noizetier.',

	// D
	'description_bloc_contenu' => 'Contenido principal de cada página.',
	'description_bloc_extra' => 'Información extra contextual  para cada página.',
	'description_bloc_navigation' => 'Informaciones de navegación propias a cada página.',
	'description_bloctexte' => 'El título es opcional. Para el texto, puede utilizar los atajos tipográficos de SPIP. ',

	// E
	'editer_noizetier_explication' => 'Selecione la página de la cual quiere configurar las noisettes ', # Ya lo dije noisette =avellana, pero lo dejo sin traducir. Si alguien piensa que es mejor traducirlo...
	'editer_noizetier_titre' => 'Gestionar las noisettes',
	'erreur_aucune_noisette_selectionnee' => 'Usted debe seleccionar una noisette !', # Prefiero no traducir noisette que sería avellana , pues es un término técnico importante del plugin, y le da el nombre al plugin.
	'explication_glisser_deposer' => 'Usted puede añadir una noisette o bien reordenarlas por simple deslizar-depositar', # MODIF
	'explication_heritages_composition' => 'Usted puede definir aquí las composiciones que serán heredadas por los objetos de la rama.', # MODIF

	// F
	'formulaire_ajouter_noisette' => 'Añadir una  noisette', # No se si poner avellana o noisette....
	'formulaire_composition' => 'identificador  de composition',
	'formulaire_composition_explication' => 'Indique una  palabra-clave única (minúsculas, sin espacio, sin guión y sin acento)  de manera que permita   identificar esta composición.<br />Por ejemplo : <i>micompo</i>.', # MODIF
	'formulaire_deplacer_bas' => 'Desplazar hacia abajo',
	'formulaire_deplacer_haut' => 'Desplazar hacia arriba',
	'formulaire_description' => 'Descripción',
	'formulaire_description_explication' => 'Puede utilizar los atajos SPIP usuales, como la baliza <multi>.', # No se si debo poner en html la baliza multi :  &lt;multi&gt;  o bien utilizar los signos  mayor que y menor que.... MODIF
	'formulaire_erreur_format_identifiant' => 'L’identifiant ne peut contenir que des minuscules sans accent, des chiffres et le caractère _ (underscore).', # El nombre de usuario solo puede contener letras minúsculas sin acento, números y el carácter _(underscore)
	'formulaire_icon' => 'Icône', # Icono
	'formulaire_icon_explication' => 'Puede poner el camino relativo hacia un icono (por ejemplo : <i>images/objet-liste-contenus.png</i>).', # MODIF
	'formulaire_identifiant_deja_pris' => 'Este nombre de usuario ya está utilizado !',
	'formulaire_modifier_composition' => 'Modificar esta composición :', # MODIF
	'formulaire_modifier_composition_heritages' => 'Modificar las herencias', # MODIF
	'formulaire_modifier_noisette' => 'Modificar esta  noisette',
	'formulaire_modifier_page' => 'Modificar esta página', # MODIF
	'formulaire_nom' => 'Título',
	'formulaire_nom_explication' => 'Puede utilizar la baliza  <multi>.', # veo que en la versión original hay un error al transformar un el signo < en su entidad html MODIF
	'formulaire_obligatoire' => 'Campos obligatorios',
	'formulaire_supprimer_noisette' => 'Suprimir esta  noisette', # Suprimir esta noisette
	'formulaire_supprimer_noisettes_page' => 'Suprimir las noisettes de esta página', # MODIF
	'formulaire_supprimer_page' => 'Suprimir esta página', # MODIF
	'formulaire_type' => 'Tipo de página',

	// I
	'icone_introuvable' => 'Icono no encontrado !',
	'ieconfig_noizetier_export_explication' => 'Exportará la configuración de las  noisettes y de las  composiciones du noiZetier.', # MODIF
	'ieconfig_noizetier_export_option' => '¿ Incluir en la exportación ?', # MODIF
	'ieconfig_non_installe' => '<b>Plugin Importador/Exportador de configuraciones :</b> este  plugin no está  installado  en vuestro sitio. No es necesario al funcionamiento del noizetier. No obstante, si está activado, usted podrá exportar e importar configuraciones de noisettes en el noizetier.',
	'ieconfig_probleme_import_config' => 'Se ha encontrado un problema  al importar la configuración del noizetier.',

	// L
	'label_niveau_titre' => 'Nivel del título :',
	'label_texte' => 'Texto :',
	'label_titre' => 'Título : ',
	'liste_pages' => 'Liste de páginas',

	// M
	'masquer' => 'Ocultar',
	'mode_noisettes' => 'Editar las noisettes',

	// N
	'ne_pas_definir_d_heritage' => 'No definir la herencia', # MODIF
	'noisette_numero' => 'noisette numero :',
	'noisettes_composition' => 'Noisettes específicas a la composición <i>@composition@</i> :', # lo que es código, no lo traduzco, lo dejo tal cual
	'noisettes_disponibles' => 'Noisettes disponibles', # MODIF
	'noisettes_page' => 'Noisettes específicas a la página  <i>@type@</i> :', # MODIF
	'noisettes_toutes_pages' => 'Noisettes comunes a todas las páginas :', # MODIF
	'noizetier' => 'noiZetier', # Dejamos {noizetier} en vez de {avellano}, para mas claridad  en  la comprensión de la programación del plugin.
	'nom_bloc_contenu' => 'Contenido',
	'nom_bloc_extra' => 'Extra',
	'nom_bloc_navigation' => 'Navegación', # Navegación
	'nom_bloctexte' => 'Bloque  de texto libre',

	// P
	'probleme_droits' => 'Usted no tiene los derechos necesarios para efectuar esta modificación.',

	// Q
	'quitter_mode_noisettes' => 'Dejar la edición de las noisettes ',

	// R
	'retour' => 'Volver',

	// S
	'suggestions' => 'Sugerencias',
];

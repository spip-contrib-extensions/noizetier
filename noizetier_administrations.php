<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Installation du schéma de données propre au plugin et gestion des migrations suivant
 * les évolutions du schéma.
 *
 * Le schéma comprend des tables et des variables de configuration propres au plugin.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 * @param string $version_cible         Version du schéma de données en fin d'upgrade
 *
 * @return void
 */
function noizetier_upgrade(string $nom_meta_base_version, string $version_cible) : void {
	$maj = [];

	// Configurations par défaut
	$config = [
		'objets_noisettes'             => [],
		'encapsulation_noisette'       => 'on',
		'ajax_noisette'                => 'on',
		'inclusion_dynamique_noisette' => 'on',
		'profondeur_max'               => '',
		'types_noisettes_masques'      => [],
		'afficher_page_inactive'       => '',
	];

	$maj['create'] = [
		['maj_tables',['spip_noizetier_pages', 'spip_types_noisettes', 'spip_noisettes']],
		['ecrire_config', 'noizetier', $config],
	];

	$maj['0.2.0'] = [
		['maj_tables',['spip_noisettes']],
	];

	$maj['0.3.0'] = [
		['sql_alter','TABLE spip_noisettes DROP COLUMN contexte'],
	];

	$maj['0.4.0'] = [
		['maj_tables',['spip_noisettes']],
	];

	$maj['0.5.0'] = [
		['maj_tables',['spip_noisettes']],
		['sql_alter', 'TABLE spip_noisettes ADD INDEX (type(255))'],
		['sql_alter', 'TABLE spip_noisettes ADD INDEX (composition(255))'],
		['sql_alter', 'TABLE spip_noisettes ADD INDEX (bloc(255))'],
		['sql_alter', 'TABLE spip_noisettes ADD INDEX (noisette(255))'],
		['sql_alter', 'TABLE spip_noisettes ADD INDEX (objet)'],
		['sql_alter', 'TABLE spip_noisettes ADD INDEX (id_objet)'],
	];

	$maj['0.6.0'] = [
		[
			'maj_060',
			[
				'objets_noisettes'             => [],
				'encapsulation_noisette'       => 'on',
				'ajax_noisette'                => 'on',
				'inclusion_dynamique_noisette' => 'on',
				'profondeur_max'               => '',
			],
		],
	];

	$maj['0.7.0'] = [
		['maj_070', $version_cible],
	];

	$maj['0.8.0'] = [
		['maj_080'],
	];

	$maj['0.9.0'] = [
		['maj_090'],
	];

	$maj['0.10.0'] = [
		['ecrire_config', 'noizetier/afficher_page_inactive', ''],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Suppression de l'ensemble du schéma de données propre au plugin, c'est-à-dire
 * les tables et les variables de configuration.
 *
 * @param string $nom_meta_base_version Nom de la meta dans laquelle sera rangée la version du schéma
 *
 * @return void
 */
function noizetier_vider_tables(string $nom_meta_base_version) : void {
	// On efface les tables du plugin
	sql_drop_table('spip_noizetier_pages');
	sql_drop_table('spip_types_noisettes');
	sql_drop_table('spip_noisettes');

	// On efface la version enregistrée du schéma des données du plugin
	effacer_meta($nom_meta_base_version);
	// On efface la configuration du plugin
	effacer_meta('noizetier');
	// On efface la variable de migration
	effacer_meta('noizetier_migration');
}

/**
 * Migration du schéma 0.5 au 0.6.
 *
 * Les actions effectuées sont les suivantes:
 * - ajout de la tables `spip_noisettes_pages` pour stocker l'ensemble des pages et compositions
 *   explicites et virtuelles.
 * - ajout du champ `encapsulation` à la table `spip_noisettes` pour déterminer si le noiZetier doit inclure
 *   la noisette concernée dans un div englobant et ajout du champ plugin pour étendre le stockage au-delà
 *   du noiZetier.
 * - mise à jour de la taille des champs type, composition et objet dans la table `spip_noisettes`
 * - ajout d'une liste de variables de configuration initialisées
 * - transfert des compositions virtuelles de la meta `noizetier_compositions` dans la nouvelle
 *   table `spip_noizetier_pages` et suppression définitive de la meta.
 *
 * @param array $config_defaut Tableau des variables de configuration intialisées.
 *
 * @return void
 */
function maj_060(array $config_defaut) : void {
	// Ajout de la tables des pages du noizetier qui contiendra pages et compositions qu'elles soient
	// explicites ou virtuelles et de la tables des types de noisette.
	include_spip('base/create');
	maj_tables(['spip_noizetier_pages', 'spip_types_noisettes']);

	// Premier chargement de la liste des types de noisettes et de la liste des pages en BD
	include_spip('inc/ncore_type_noisette');
	type_noisette_charger('noizetier', true);
	include_spip('inc/noizetier_page');
	page_noizetier_charger(true);

	// Modification de la table spip_noisettes
	// -- Changement du nom du champ 'noisette' en 'type_noisette' et de sa taille
	sql_alter("TABLE spip_noisettes CHANGE noisette type_noisette varchar(255) DEFAULT '' NOT NULL");
	// -- Changement du nom du champ 'rang' en 'rang_noisette'
	sql_alter('TABLE spip_noisettes CHANGE rang rang_noisette smallint DEFAULT 1 NOT NULL');
	// -- Ajout de la colonne 'plugin' qui vaut 'noizetier' pour ce plugin.
	sql_alter("TABLE spip_noisettes ADD plugin varchar(30) DEFAULT '' NOT NULL AFTER id_noisette");
	// -- Ajouts des colonnes relatives au conteneur : 'id_conteneur', 'conteneur' et 'est_conteneur'.
	sql_alter("TABLE spip_noisettes ADD id_conteneur varchar(255) DEFAULT '' NOT NULL AFTER plugin");
	sql_alter("TABLE spip_noisettes ADD conteneur text DEFAULT '' NOT NULL AFTER id_conteneur");
	sql_alter("TABLE spip_noisettes ADD est_conteneur varchar(3) DEFAULT 'non' NOT NULL AFTER type_noisette");
	// -- Ajout de la colonne 'encapsulation' qui indique pour chaque noisette si le noiZetier doit l'inclure dans une capsule
	//    englobante ou pas. Le champ prend les valeurs 'on', '' ou 'defaut' qui indique qu'il faut prendre
	//    en compte la valeur configurée par défaut (configuration du noizetier).
	sql_alter("TABLE spip_noisettes ADD encapsulation varchar(6) DEFAULT 'defaut' NOT NULL AFTER parametres");
	// -- Ajout de la colonne 'profondeur' à la valeur par défaut 0
	//    dans les versions précédentes du plugin.
	sql_alter('TABLE spip_noisettes ADD profondeur smallint DEFAULT 0 NOT NULL AFTER css');
	// -- Suppression des index pour des colonnes dont on va modifier la taille ou le type
	sql_alter('TABLE spip_noisettes DROP INDEX type');
	sql_alter('TABLE spip_noisettes DROP INDEX composition');
	sql_alter('TABLE spip_noisettes DROP INDEX bloc');
	sql_alter('TABLE spip_noisettes DROP INDEX noisette');
	// Mise à jour des tailles des colonnes type, composition et objet pour cohérence
	sql_alter("TABLE spip_noisettes MODIFY bloc varchar(255) DEFAULT '' NOT NULL");
	sql_alter('TABLE spip_noisettes MODIFY type varchar(127) NOT NULL');
	sql_alter('TABLE spip_noisettes MODIFY composition varchar(127) NOT NULL');
	sql_alter('TABLE spip_noisettes MODIFY objet varchar(25) NOT NULL');
	// -- Création des index détruits précédemment et des nouveaux index plugin et conteneur
	sql_alter('TABLE spip_noisettes ADD INDEX type (type)');
	sql_alter('TABLE spip_noisettes ADD INDEX composition (composition)');
	sql_alter('TABLE spip_noisettes ADD INDEX bloc (bloc)');
	sql_alter('TABLE spip_noisettes ADD INDEX type_noisette (type_noisette)');
	sql_alter('TABLE spip_noisettes ADD INDEX plugin (plugin)');
	sql_alter('TABLE spip_noisettes ADD INDEX id_conteneur (id_conteneur)');
	sql_alter('TABLE spip_noisettes ADD INDEX rang_noisette (rang_noisette)');
	// -- Remplissage de la nouvelle colonne plugin avec la valeur 'noizetier'
	//    et des colonnes id_conteneur et conteneur à partir des autres colonnes existantes.
	//    La colonne est_conteneur est déjà initialisée à 'non' et cela suffit car en v2 il n'existe pas de
	//    noisette conteneur.
	$from = 'spip_noisettes';
	$noisettes = sql_allfetsel('*', $from);
	if ($noisettes) {
		include_spip('inc/ncore_conteneur');
		include_spip('inc/noizetier_conteneur');
		foreach ($noisettes as $_cle => $_noisette) {
			// C'est le plugin noizetier
			$noisettes[$_cle]['plugin'] = 'noizetier';
			// On calcule le conteneur au format tableau et on appelle la fonction de service de construction
			// de l'id du conteneur
			$page = [];
			if (!empty($_noisette['objet']) and !empty($_noisette['id_objet']) and (int) ($_noisette['id_objet'])) {
				$page['objet'] = $_noisette['objet'];
				$page['id_objet'] = $_noisette['id_objet'];
			} else {
				$page = $_noisette['type'] . ($_noisette['composition'] ? "-{$_noisette['composition']}" : '');
			}
			$noisettes[$_cle]['id_conteneur'] = conteneur_noizetier_composer($page, $_noisette['bloc']);
			$noisettes[$_cle]['conteneur'] = serialize(
				conteneur_construire(
					'noizetier',
					$noisettes[$_cle]['id_conteneur']
				)
			);
		}
		sql_replace_multi($from, $noisettes);
	}

	// Mise à jour de la configuration du plugin
	include_spip('inc/config');
	$config = lire_config('noizetier', []);
	if ($config and isset($config['objets_noisettes'])) {
		$config_defaut['objets_noisettes'] = $config['objets_noisettes'];
	}
	ecrire_config('noizetier', $config_defaut);

	// Suppression des caches devenus inutiles
	include_spip('inc/flock');
	supprimer_fichier(_DIR_CACHE . 'noisettes_descriptions.php');
	supprimer_fichier(_DIR_CACHE . 'noisettes_ajax.php');
	supprimer_fichier(_DIR_CACHE . 'noisettes_contextes.php');
	supprimer_fichier(_DIR_CACHE . 'noisettes_inclusions.php');

	// Déplacement de la liste des compositions virtuelles dans la table 'spip_noisettes_pages'
	$compositions = lire_config('noizetier_compositions', []);
	if ($compositions) {
		$compositions_060 = [];
		foreach ($compositions as $_type => $_compositions) {
			foreach ($_compositions as $_composition => $_description) {
				// Initialisation par défaut de la composition virtuelle sachant que le type et la composition ne sont
				// jamais vides.
				$page = "{$_type}-{$_composition}";
				$description = [
					'page'           => $page,
					'type'           => $_type,
					'composition'    => $_composition,
					'nom'            => $page,
					'description'    => '',
					'icon'           => 'composition-xx.svg',
					'blocs_exclus'   => [],
					'necessite'      => [],
					'est_active'     => 'oui',
					'branche'        => [],
					'est_virtuelle'  => 'oui',
					'est_page_objet' => 'non',
					'signature'      => '',
				];
				// Mise à jour du nom si défini
				if (!empty($_description['nom'])) {
					$description['nom'] = $_description['nom'];
				}
				// Mise à jour de la description si définie
				if (!empty($_description['description'])) {
					$description['description'] = $_description['description'];
				}
				// Mise à jour de l'icone si défini
				if (!empty($_description['icon'])) {
					$description['icon'] = $_description['icon'];
				}
				// Traitement des necessite pour identifier l'activité de la page
				if (!empty($_description['necessite'])) {
					foreach ($_description['necessite'] as $_plugin_necessite) {
						if (!defined('_DIR_PLUGIN_' . strtoupper($_plugin_necessite))) {
							$description['est_active'] = 'non';
							break;
						}
					}
				}
				// Blocs, necessite et branche: des tableaux à sérialiser
				// TODO : est-il possible d'avoir ces données dans une composition virtuelle v2 ?
				$description['blocs_exclus'] = isset($_description['blocs_exclus'])
					? serialize($_description['blocs_exclus'])
					: serialize([]);
				$description['necessite'] = isset($_description['necessite'])
					? serialize($_description['necessite'])
					: serialize([]);
				$description['branche'] = isset($_description['branche'])
					? serialize($_description['branche'])
					: serialize([]);
				// Indicateur de type d'objet
				include_spip('base/objets');
				$tables_objets = array_keys(lister_tables_objets_sql());
				if (in_array(table_objet_sql($_type), $tables_objets)) {
					$description['est_page_objet'] = 'oui';
				}
				$compositions_060[] = $description;
			}
		}
		if ($compositions_060) {
			if (sql_preferer_transaction()) {
				sql_demarrer_transaction();
			}
			sql_insertq_multi('spip_noizetier_pages', $compositions_060);
			if (sql_preferer_transaction()) {
				sql_terminer_transaction();
			}
		}
	}
	// On efface la meta des compositions maintenant que celles-ci sont stockées
	// dans une table dédiées aux pages du noizetier
	effacer_meta('noizetier_compositions');
}

/**
 * Migration du schéma 0.6 au 0.7.
 *
 * Les actions effectuées sont les suivantes:
 * - ajout du champ `css_saisies` à la table `spip_types_noisettes` pour stocker les saisies qui permettent d'éditer
 *   les styles de la capsule ou de la noisette conteneur.
 *
 * @return void
 */
function maj_070() : void {
	// Ajout du champ css à la table spip_types_noisettes.
	include_spip('base/create');
	sql_alter("TABLE spip_types_noisettes ADD css_saisies text DEFAULT '' NOT NULL AFTER parametres");
	sql_alter("TABLE spip_noisettes ADD css_saisies text DEFAULT '' NOT NULL AFTER css");

	// Création d'un indicateur de migration à lancer en meta
	include_spip('inc/config');
	ecrire_config('noizetier_migration', '0.7.0');
}

/**
 * Migration du schéma 0.7 au 0.8.
 *
 * Les actions effectuées sont les suivantes:
 * - ajout du champ `groupes` à la table `spip_types_noisettes` pour stocker affectations des paramètres à des
 *   groupes représentés par des fieldsets ou des onglets dans le formulaire d'édition
 * - rechargement des types de noisettes pour initialiser ce nouveau champ.
 *
 * @return void
 */
function maj_080() : void {
	// Ajout du champ css à la table spip_types_noisettes.
	include_spip('base/create');
	sql_alter("TABLE spip_types_noisettes ADD groupes text DEFAULT '' NOT NULL AFTER parametres");
	maj_tables(['spip_types_noisettes']);

	// Rechargement de la liste des types de noisettes en BD
	include_spip('inc/ncore_type_noisette');
	type_noisette_charger('noizetier', true);
}

/**
 * Migration du schéma 0.8 au 0.9.
 *
 * Les actions effectuées sont les suivantes:
 * - ajout du champ `categories` à la table `spip_types_noisettes`
 * - rechargement des types de noisettes pour initialiser ce nouveau champ.
 *
 * @return void
 */
function maj_090() : void {
	// Ajout du champ css à la table spip_types_noisettes.
	include_spip('base/create');
	sql_alter("TABLE spip_types_noisettes ADD categories text DEFAULT '' NOT NULL AFTER css_saisies");
	maj_tables(['spip_types_noisettes']);

	// Rechargement de la liste des types de noisettes en BD
	include_spip('inc/ncore_type_noisette');
	type_noisette_charger('noizetier', true);
}

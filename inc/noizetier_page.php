<?php
/**
 * Ce fichier contient l'API de gestion des pages et compositions configurables par le noiZetier.
 *
 * @package SPIP\NOIZETIER\PAGE\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Charge les pages disponibles pour le noiZetier à partir des fichiers YAML ou XML voire HTML.
 *
 * @api
 *
 * @param null|bool   $recharger         Indique si il faut forcer le rechargement à partir des fichiers descriptifs ou si
 *                                       le chargement ne se fait que si la description a changé
 * @param null|string $id_page_a_charger Identifiant d'une page à charger ou vide si toutes les pages sont à charger
 *
 * @return bool `true` si le chargement s'est bien passé, `false` sinon.
 */
function page_noizetier_charger(?bool $recharger = false, ?string $id_page_a_charger = '') : bool {
	// Retour de la fonction
	$retour = false;

	// Initialiser les blocs par défaut
	include_spip('inc/noizetier_bloc');
	$options['blocs_defaut'] = bloc_noizetier_lister_defaut();

	// Choisir le bon répertoire des pages
	$options['repertoire_pages'] = page_noizetier_initialiser_dossier();

	// Initialiser le contexte de rechargement
	$forcer_chargement = $recharger;

	// Seules les pages explicites sont concernées.
	$filtres = ['est_virtuelle' => 'non'];
	if ($id_page_a_charger) {
		// On limite la recherche à la page demandée
		$filtres['page'] = $id_page_a_charger;
	}

	// Déterminer le ou les fichiers HTML correspondant auxpages ou à la page à recharger
	$fichiers = $id_page_a_charger
		? [find_in_path("{$options['repertoire_pages']}{$id_page_a_charger}.html")]
		: find_all_in_path($options['repertoire_pages'], '.+[.]html$');

	// On recherche les pages et les compositions explicites par le fichier HTML en premier
	// Si on le trouve, on récupère la configuration du fichier XML ou YAML.
	if ($fichiers) {
		// Récupération pour les divers traitements qui suivent:
		// - des signatures md5 des pages déjà enregistrées pour déterminer si les fichiers YAML/XML
		//   ont subi des changements.
		// - des blocs exclus qui sont éditables après chargement, il faut donc conserver les modifications éventuelles.
		// - des plugins nécessités et des indicateurs d'activité (voir fin de traitement).
		$pages_nouvelles = $pages_modifiees = $pages_obsoletes = [];
		$signatures = [];

		// On construit le tableau des pages déjà enregistrées en base indexé par identifiant de page.
		if ($pages_existantes = page_noizetier_repertorier([], $filtres)) {
			// Si on force le rechargement il est inutile de gérer les signatures, les indicateurs d'activité
			// et les pages modifiées ou obsolètes.
			if (!$forcer_chargement) {
				$signatures = array_column($pages_existantes, 'signature', 'page');
				// On initialise la liste des pages à supprimer avec l'ensemble des pages non virtuelles
				$pages_obsoletes = $signatures ? array_keys($signatures) : [];
			}
		}

		foreach ($fichiers as $_chemin) {
			$id_page = basename($_chemin, '.html');
			$dossier = dirname($_chemin);
			$est_composition = (page_noizetier_extraire_composition($id_page) != '');
			// Exclure certaines pages :
			// -- celles du privé situes dans prive/contenu
			// -- page liée au plugin Zpip en v1
			// -- z_apl liée aux plugins Zpip v1 et Zcore
			// -- les compositions explicites si le plugin Compositions n'est pas activé
			if ((substr($dossier, -13) !== 'prive/contenu')
			and (($id_page !== 'page') or !defined('_DIR_PLUGIN_Z'))
			and (($id_page !== 'z_apl') or (!defined('_DIR_PLUGIN_Z') and !defined('_DIR_PLUGIN_ZCORE')))
			and (!$est_composition or ($est_composition and defined('_DIR_PLUGIN_COMPOSITIONS')))) {
				// On passe le md5 de la page si il existe sinon la chaîne vide. Cela permet de déterminer
				// si on doit ajouter la page ou la mettre à jour.
				// Si le md5 est le même et qu'il n'est donc pas utile de recharger la page, la configuration
				// retournée est vide.
				$options['md5'] = $signatures[$id_page] ?? '';
				$options['recharger'] = $forcer_chargement;
				if ($configuration = phraser_fichier_descriptif_page($id_page, $options)) {
					if (empty($configuration['identique'])) {
						// On met à jour les blocs exclus avec la sauvegarde effectuée au préalable (si la page
						// existait déjà en base).
						if (isset($pages_existantes[$id_page])) {
							$configuration['blocs_exclus'] = $pages_existantes[$id_page]['blocs_exclus'];
						}
						// On détermine si la page est nouvelle ou modifiée.
						// En mode rechargement forcé toute page est considérée comme nouvelle.
						if (!$options['md5'] or $forcer_chargement) {
							// La page est soit nouvelle soit on est en mode rechargement forcé:
							// => il faut la rajouter dans la table.
							$pages_nouvelles[] = $configuration;
						} else {
							// La configuration stockée dans la table a été modifiée et pas de forçage du rechargement:
							// => il faut mettre à jour la page dans la table.
							$pages_modifiees[] = $configuration;
							// => il faut donc la supprimer de la liste des pages obsolètes
							$pages_obsoletes = array_diff($pages_obsoletes, [$id_page]);
						}
					} else {
						// La page n'a pas changée et n'a donc pas été réchargée:
						// => Il faut donc juste indiquer qu'elle n'est pas obsolète.
						$pages_obsoletes = array_diff($pages_obsoletes, [$id_page]);
					}
				} else {
					// Il y a eu une erreur sur lors du rechargement de la page.
					// Ce peut être en particulier le cas où une page HTML sans XML n'est plus détectée car le
					// paramètre _NOIZETIER_LISTER_PAGES_SANS_XML a été positionné de true à false.
					// => il faut donc ne rien faire pour laisser la page dans les obsolètes
					continue;
				}
			}
		}

		// On complète la liste des pages à changer avec les pages dont l'indicateur d'activité est modifié suite
		// à l'activation ou à la désactivation d'un plugin (le fichier XML/YAML lui n'a pas changé). Il est inutile de
		// le faire si on recharge tout.
		// -- on cherche ces pages en excluant les pages obsolètes et celles à changer qui ont déjà recalculé
		//    l'indicateur lors de la lecture du fichier XML/YAML.
		if (!$forcer_chargement) {
			$pages_exclues = $pages_modifiees
				? array_merge(array_column($pages_modifiees, 'page'), $pages_obsoletes)
				: $pages_obsoletes;
			$pages_a_verifier = $pages_exclues
				? array_diff_key($pages_existantes, array_flip($pages_exclues))
				: $pages_existantes;

			if ($pages_a_verifier) {
				foreach ($pages_a_verifier as $_description) {
					$est_active = 'oui';
					$plugins_necessites = unserialize($_description['necessite']);
					if ($plugins_necessites) {
						foreach ($plugins_necessites as $_plugin_necessite) {
							if (!defined('_DIR_PLUGIN_' . strtoupper($_plugin_necessite))) {
								$est_active = 'non';
								break;
							}
						}
					}
					if ($est_active != $_description['est_active']) {
						// On stocke la mise à jour dans les types à changer.
						$_description['est_active'] = $est_active;
						$pages_modifiees[] = $_description;
					}
				}
			}
		}

		// Mise à jour de la table des pages
		// -- Initialiser la table
		$from = 'spip_noizetier_pages';

		// -- Suppression des pages obsolètes ou de toute les pages non virtuelles si on est en mode
		//    rechargement forcé.
		if (sql_preferer_transaction()) {
			sql_demarrer_transaction();
		}
		if ($pages_obsoletes) {
			sql_delete($from, sql_in('page', $pages_obsoletes));
		} elseif ($forcer_chargement) {
			// -- Initialiser le where des pages non virtuelles
			$where = calculer_condition_par_filtre($filtres);
			sql_delete($from, $where);
		}
		// -- Update des pages modifiées
		if ($pages_modifiees) {
			sql_replace_multi($from, $pages_modifiees);
		}
		// -- Insertion des nouvelles pages
		if ($pages_nouvelles) {
			sql_insertq_multi($from, $pages_nouvelles);
		}

		// Maintenant que les pages non virtuelles sont mises à jour il faut propager certaines modifications sur les
		// pages composition virtuelles : le necessite et l'indicateur d'activité.
		// Il suffit de le faire pour les pages modifiées uniquement car les pages nouvelles n'ont pas encore pu être
		// dérivées en composition virtuelle ou pour les pages nouvelles si on a forcé le chargement.
		page_noizetier_virtuelle_propager($forcer_chargement ? $pages_nouvelles : $pages_modifiees);

		if (sql_preferer_transaction()) {
			sql_terminer_transaction();
		}

		$retour = true;
	}

	return $retour;
}

/**
 * Initialise avec des valeurs par défaut la description d'une page connue par son identifiant.
 *
 * @api
 *
 * @param string $id_page Identifiant de la page
 *
 * @return array La description initialisée.
 */
function page_noizetier_initialiser(string $id_page) : array {
	// Initialisation de la description par défaut de la page
	return [
		'page'           => $id_page,
		'type'           => page_noizetier_extraire_type($id_page),
		'composition'    => page_noizetier_extraire_composition($id_page),
		'nom'            => $id_page,
		'description'    => '',
		'icon'           => 'page-xx.svg',
		'blocs_exclus'   => [],
		'necessite'      => [],
		'est_active'     => 'oui',
		'branche'        => [],
		'est_virtuelle'  => 'non',
		'est_page_objet' => 'non',
		'signature'      => '',
	];
}

/**
 * Propage éventuellement les modifications des pages non virtuelles vers les compositions virtuelles associées.
 * La mise à jour concerne les plugins necessités et l'indicateur d'activité.
 *
 * @param array $pages Tableau de la description complète des pages et compositions non virtuelles modifiées.
 *
 * @return void
 */
function page_noizetier_virtuelle_propager(array $pages) : void {
	if ($pages) {
		// On filtre uniquement les pages non composition car les compositions virtuelles ne dépendent que d'elles.
		$pages_non_composition = array_column(
			array_filter($pages, function ($v) {
				return empty($v['composition']);
			}),
			null,
			'type'
		);

		if ($pages_non_composition) {
			// On récupère toutes les pages virtuelles correspondant aux pages non composition modifiées.
			$filtres = [
				'est_virtuelle' => 'oui',
				'type'          => array_keys($pages_non_composition)
			];
			$pages_virtuelles = page_noizetier_repertorier([], $filtres, false);

			if ($pages_virtuelles) {
				// On positionne les necessite et les indicateurs d'activité avec ceux des pages modifiées
				foreach ($pages_virtuelles as $_cle => $_page_virtuelle) {
					$type = $_page_virtuelle['type'];
					$pages_virtuelles[$_cle]['necessite'] = $pages_non_composition[$type]['necessite'];
					$pages_virtuelles[$_cle]['est_active'] = $pages_non_composition[$type]['est_active'];
				}

				// Mise à jour des compositions virtuelles
				sql_replace_multi('spip_noizetier_pages', $pages_virtuelles);
			}
		}
	}
}

/**
 * Retourne la description complète de la page, de la composition explicite ou de la composition virtuelle demandée ou
 * une information donnée uniquement.
 * La description est stockée en base de données, certains champs sont recalculés avant d'être fournis.
 *
 * @api
 *
 * @uses page_noizetier_lister_blocs()
 *
 * @param string            $id_page      Identifiant de la page ou de la composition.
 * @param null|array|string $informations Liste des champs à renvoyer. Si vide la fonction renvoie tous les champs. Il est possible de demander
 *                                        un seul champ sous la forme d'une chaine et pas de tableau.
 * @param null|bool         $traiter_typo Indique si les données textuelles doivent être retournées brutes ou si elles doivent être traitées
 *                                        en utilisant la fonction typo.
 *                                        Les champs sérialisés sont toujours désérialisés.
 *
 * @return array|string La description complète ou un champ précis demandé pour une page donnée. Les champs
 *                      de type tableau sont systématiquement désérialisés et si demandé, les champs textuels peuvent être
 *                      traités avec la fonction typo().
 */
function page_noizetier_lire(string $id_page, $informations = [], ?bool $traiter_typo = false) {
	static $description_page = [];

	// On vérifie si la page demandée n'est pas déjà stockée : si oui, la description sera utilisée
	if (isset($description_page[$traiter_typo][$id_page])) {
		$description = $description_page[$traiter_typo][$id_page];
	} else {
		$description = [];
	}

	if (!$description) {
		// Chargement de toute la configuration de la page en base de données.
		$description = sql_fetsel('*', 'spip_noizetier_pages', ['page=' . sql_quote($id_page)]);

		// Sauvegarde de la description de la page pour une consultation ultérieure dans le même hit.
		if ($description) {
			// Traitements des champs textuels
			if ($traiter_typo) {
				$description['nom'] = typo($description['nom']);
				if ($description['description']) {
					$description['description'] = typo($description['description']);
				}
			}

			// Traitements des champs tableaux sérialisés
			$description['blocs_exclus'] = unserialize($description['blocs_exclus']);
			$description['necessite'] = unserialize($description['necessite']);
			$description['branche'] = unserialize($description['branche']);

			// Calcul des blocs
			$description['blocs'] = page_noizetier_lister_blocs($id_page, $description['blocs_exclus']);

			// Le nombre de noisettes associées à la page.
			$description['noisettes'] = 0;
			$from = ['spip_noisettes'];
			// -- Construction du where identifiant précisément le type et la composition de la page
			$where = [
				'plugin=' . sql_quote('noizetier'),
				'type=' . sql_quote($description['type']),
				'composition=' . sql_quote($description['composition'])
			];
			if ($noisettes = sql_countsel($from, $where)) {
				$description['noisettes'] = $noisettes;
			}

			// Stockage de la description
			$description_page[$traiter_typo][$id_page] = $description;
		} else {
			// En cas d'erreur stocker une description vide
			$description_page[$traiter_typo][$id_page] = [];
		}
	}

	// On ne retourne maintenant que les champs demandés.
	if ($description and $informations) {
		// Extraction des seules informations demandées.
		// -- si on demande une information unique on renvoie la valeur simple, sinon on renvoie un tableau.
		// -- si une information n'est pas un champ valide elle n'est pas renvoyée sans renvoyer d'erreur.
		if (is_array($informations)) {
			if (count($informations) == 1) {
				// Tableau d'une seule information : on revient à une chaine unique.
				$informations = array_shift($informations);
			} else {
				// Tableau des informations valides
				$description = array_intersect_key($description, array_flip($informations));
			}
		}

		if (is_string($informations)) {
			// Valeur unique demandée.
			$description = isset($description[$informations]) ? $description[$informations] : '';
		}
	}

	return $description;
}

/**
 * Retourne la description complète ou partielle d'une liste de pages explicites, compositions explicite et
 * compositions virtuelles filtrées sur une liste de critères.
 *
 * @api
 *
 * @param array|string $information      Information spécifique à retourner ou vide pour retourner toute la description. L'information peut être une
 *                                       chaine correspondant à un champ de la page ou un tableau avec une liste de champs.
 * @param null|array   $filtres          Tableau associatif `[champ] = valeur` de critères de filtres sur les descriptions de types de noisette.
 *                                       Le seul opérateur possible est l'égalité.
 * @param null|bool    $indexer_par_page Indique si le tableau doit être indexé par d'identifiant de page ou pas. `true` par défaut.
 *
 * @return array Tableau des descriptions des pages et compositions trouvées indexé par l'identifiant de la page ou par
 *               entier de 0 à n.
 */
function page_noizetier_repertorier($information = [], ?array $filtres = [], ?bool $indexer_par_page = true) : array {
	// Initialiser la sortie.
	$pages = [];

	// On initialise la liste des champs de la table des pages
	$trouver_table = charger_fonction('trouver_table', 'base');
	$table = $trouver_table('spip_noizetier_pages');

	// On vérifie les champs à retourner et on calcule le contenu du select.
	$champs = array_diff(array_keys($table['field']), ['maj']);
	$information_valide = true;
	$information_unique = false;
	if (!$information) {
		$select = $champs;
	} else {
		if (is_string($information)) {
			$select = [$information];
			$information_unique = true;
		} else {
			$select = $information;
		}

		// On vérifie que les champs sont bien valides.
		foreach ($select as $_champ) {
			if (!in_array($_champ, $champs)) {
				$information_valide = false;
				break;
			}
		}

		// On ajoute toujours l'identifiant page car il sert à l'indexation du tableau de sortie.
		if ($indexer_par_page
		and $information_valide
		and !in_array('page', $select)) {
			$select[] = 'page';
		}
	}

	if ($information_valide) {
		// On calcule le where à partir des filtres sachant que tous les champs sont des chaines.
		$where = calculer_condition_par_filtre($filtres);

		// Chargement des pages et compositions.
		$pages = sql_allfetsel($select, 'spip_noizetier_pages', $where);

		// On renvoie l'information demandée indexée par page si demandé ainsi.
		if ($indexer_par_page) {
			if ($information_unique) {
				$pages = array_column($pages, $information, 'page');
			} else {
				$pages = array_column($pages, null, 'page');
				if ($information and !in_array('page', $information)) {
					// On supprime le champ 'page' qui n'a pas été demandé
					foreach ($pages as $_id_page => $_page) {
						unset($pages[$_id_page]['page']);
					}
				}
			}
		}
	}

	return $pages;
}

/**
 * Liste, pour une page donnée, les blocs pouvant accepter des noisettes.
 *
 * @api
 *
 * @param string     $id_page      Identifiant de la page.
 * @param null|array $blocs_exclus Liste des blocs exclus si ils sont connus avant l'appel. Sinon, ils seront récupérés
 *                                 dans le déroulement de la fonction.
 *
 * @return array Liste des blocs
 */
function page_noizetier_lister_blocs(string $id_page, ?array $blocs_exclus = []) : array {
	// Initialisation des blocs avec la liste des blocs par défaut
	include_spip('inc/noizetier_bloc');
	$blocs = bloc_noizetier_lister_defaut();

	// Si la liste des blocs exclus n'a pas été passé en argument on les cherche dans la configuration
	// de la page
	if (!$blocs_exclus) {
		include_spip('base/abstract_sql');
		$where = ['page=' . sql_quote($id_page)];
		$blocs_exclus = sql_getfetsel('blocs_exclus', 'spip_noizetier_pages', $where);
		if (null !== $blocs_exclus) {
			$blocs_exclus = unserialize($blocs_exclus);
		} else {
			$blocs_exclus = [];
		}
	}

	if ($blocs_exclus) {
		$blocs = array_diff($blocs, $blocs_exclus);
		sort($blocs);
	}

	return $blocs;
}

/**
 * Renvoie le type d'une page à partir de son identifiant.
 *
 * On gère aussi le cas de Zpip v1 où page-xxxx désigne une page et non une composition.
 * Dans ce cas, on doit donc obtenir un type = xxxx.
 *
 * @api
 *
 * @param string $id_page Identifiant de la page.
 *
 * @return string Le type de la page choisie, c'est-à-dire:
 *                - soit l'identifiant complet de la page,
 *                - soit le mot précédent le tiret dans le cas d'une composition.
 */
function page_noizetier_extraire_type(string $id_page) : string {
	$composants = explode('-', $id_page, 2);
	if ($composants[0] === 'page') {
		$type = $composants[1] ?? '';
	} else {
		$type = $composants[0];
	}

	return $type;
}

/**
 * Détermine, à partir de son identifiant, la composition d'une page si elle existe.
 *
 * On gère aussi le cas de Zpip v1 où page-xxxx désigne une page et non une composition.
 * Dans ce cas, on doit donc obtenir une composition vide.
 *
 * @api
 *
 * @param string $id_page Identifiant de la page.
 *
 * @return string La composition de la page choisie, à savoir, le mot suivant le premier le tiret,
 *                ou la chaine vide sinon.
 */
function page_noizetier_extraire_composition(string $id_page) : string {
	$composants = explode('-', $id_page, 2);
	if ($composants[0] === 'page') {
		$composition = '';
	} else {
		$composition = $composants[1] ?? '';
	}

	return $composition;
}

/**
 * Détermine si les compositions sont possibles sur un type de page.
 *
 * @api
 *
 * @param string $type Identifiant du type de page.
 *
 * @return bool `true` si les compositions sont autorisées, `false` sinon.
 */
function page_noizetier_composition_activee(string $type) : bool {
	$est_activee = false;

	if (defined('_DIR_PLUGIN_COMPOSITIONS')) {
		include_spip('compositions_fonctions');
		if (in_array($type, compositions_objets_actives())) {
			$est_activee = true;
		}
	}

	return $est_activee;
}

/**
 * Déterminer le répertoire dans lequel le NoiZetier peut lister les pages pouvant supporter
 * l'insertion de noisettes.
 *
 * @api
 *
 * @return string Le répertoire des pages sous la forme dossier/.
 */
function page_noizetier_initialiser_dossier() : string {
	if (defined('_NOIZETIER_REPERTOIRE_PAGES')) {
		$repertoire_pages = _NOIZETIER_REPERTOIRE_PAGES;
	} elseif (isset($GLOBALS['z_blocs'])) {
		$premier_bloc = reset($GLOBALS['z_blocs']);
		$repertoire_pages = "{$premier_bloc}/";
	} else {
		$repertoire_pages = 'contenu/';
	}

	return $repertoire_pages;
}

/**
 * Détermine, pour une page donnée, la liste des blocs ayant des noisettes incluses et renvoie leur nombre.
 *
 * @api
 *
 * @param string $id_page L'identifiant de la page ou de la composition.
 *
 * @return array Tableau des nombre de noisettes incluses par bloc de la forme [bloc] = nombre de noisettes.
 */
function page_noizetier_compter_noisettes(string $id_page) : array {
	static $blocs_compteur = [];

	if (!isset($blocs_compteur[$id_page])) {
		// Initialisation des compteurs par bloc
		$nb_noisettes = [];

		// Le nombre de noisettes par bloc doit être calculé par une lecture de la table spip_noisettes.
		$from = ['spip_noisettes'];
		$select = ['bloc', "count(type_noisette) as 'noisettes'"];
		// -- Construction du where identifiant précisément le type et la composition de la page
		$where = [
			'plugin=' . sql_quote('noizetier'),
			'type=' . sql_quote(page_noizetier_extraire_type($id_page)),
			'composition=' . sql_quote(page_noizetier_extraire_composition($id_page))
		];
		$group = ['bloc'];
		$blocs_non_vides = sql_allfetsel($select, $from, $where, $group);
		if ($blocs_non_vides) {
			// On formate le tableau [bloc] = nb noisettes
			$nb_noisettes = array_column($blocs_non_vides, 'noisettes', 'bloc');
		}

		// Sauvegarde des compteurs pour les blocs concernés.
		$blocs_compteur[$id_page] = $nb_noisettes;
	}

	return $blocs_compteur[$id_page];
}

/**
 * Phrase le fichier XML ou YAML des pages et compositions configurables par le noiZetier et renvoie
 * un tableau des caractéristiques complètes.
 *
 * @internal
 *
 * @uses page_noizetier_initialiser_dossier()
 * @uses bloc_noizetier_lister_defaut()
 *
 * @param string     $id_page Identifiant de la page
 * @param null|array $options Options de phrasage qui permettent en particulier d'accélérer les traitements
 *                            (répertoire des pages, blocs par défaut, indicateur de forcage du chargement...).
 *
 * @return array Description de la page
 */
function phraser_fichier_descriptif_page(string $id_page, ?array $options = []) : array {
	// Initialisation de la description
	$description = [];

	// Choisir le bon répertoire des pages
	if (empty($options['repertoire_pages'])) {
		$options['repertoire_pages'] = page_noizetier_initialiser_dossier();
	}

	// Initialiser les blocs par défaut
	if (empty($options['blocs_defaut'])) {
		include_spip('inc/noizetier_bloc');
		$options['blocs_defaut'] = bloc_noizetier_lister_defaut();
	}

	// Initialiser le contexte de chargement
	if (!isset($options['recharger'])) {
		$options['recharger'] = false;
	}
	if (!isset($options['md5']) or $options['recharger']) {
		$options['md5'] = '';
	}

	// Initialisation de la description par défaut de la page
	$description_defaut = page_noizetier_initialiser($id_page);

	// Recherche des pages ou compositions explicites suivant le processus :
	// a- Le fichier YAML est recherché en premier,
	// b- ensuite le fichier XML pour compatibilité ascendante.
	// c- enfin, si il n'y a ni YAML, ni XML et que le mode le permet, on renvoie une description standard minimale
	//    basée sur le fichier HTML uniquement
	$md5 = '';
	if ($fichier = find_in_path("{$options['repertoire_pages']}{$id_page}.yaml")) {
		// 1a- il y a un fichier YAML de configuration, on vérifie le md5 avant de charger le contenu.
		//     Un YAML de page ne peut pas contenir d'inclusion YAML.
		$md5 = md5_file($fichier);
		if ($md5 != $options['md5']) {
			include_spip('inc/yaml');
			$description = yaml_decode_file($fichier);
		}
	} elseif ($fichier = find_in_path("{$options['repertoire_pages']}{$id_page}.xml")) {
		// 1b- il y a un fichier XML de configuration, on vérifie le md5 avant de charger le contenu.
		//     on extrait et on parse le XML de configuration en tenant compte que ce peut être
		//     celui d'une page ou d'une composition, ce qui change la balise englobante.
		$md5 = md5_file($fichier);
		if ($md5 != $options['md5']) {
			include_spip('inc/xml');
			if ($xml = spip_xml_load($fichier, false)
			and (isset($xml['page']) or isset($xml['composition']))) {
				$xml = isset($xml['page']) ? reset($xml['page']) : reset($xml['composition']);
				// Titre (nom), description et icone
				if (isset($xml['nom'])) {
					$description['nom'] = spip_xml_aplatit($xml['nom']);
				}
				if (isset($xml['description'])) {
					$description['description'] = spip_xml_aplatit($xml['description']);
				}
				if (isset($xml['icon'])) {
					$description['icon'] = reset($xml['icon']);
				}

				// Liste des blocs autorisés pour la page. On vérifie que les blocs configurés sont bien dans
				// la liste des blocs par défaut et on calcule les blocs exclus qui sont les seuls insérés en base.
				$blocs_inclus = [];
				if (spip_xml_match_nodes(',^bloc,', $xml, $blocs)) {
					foreach (array_keys($blocs) as $_bloc) {
						[, $attributs] = spip_xml_decompose_tag($_bloc);
						$blocs_inclus[] = $attributs['id'];
					}
				}
				if ($blocs_inclus) {
					$description['blocs_exclus'] = array_values(
						array_diff(
							$options['blocs_defaut'],
							array_intersect($options['blocs_defaut'], $blocs_inclus)
						)
					);
				}

				// Liste des plugins nécessaires pour utiliser la page
				if (spip_xml_match_nodes(',^necessite,', $xml, $necessites)) {
					$description['necessite'] = [];
					foreach (array_keys($necessites) as $_necessite) {
						[, $attributs] = spip_xml_decompose_tag($_necessite);
						$description['necessite'][] = $attributs['id'];
					}
				}

				// Liste des héritages
				if (spip_xml_match_nodes(',^branche,', $xml, $branches)) {
					$description['branche'] = [];
					foreach (array_keys($branches) as $_branche) {
						[, $attributs] = spip_xml_decompose_tag($_branche);
						$description['branche'][$attributs['type']] = $attributs['composition'];
					}
				}
			}
		}
	} elseif (defined('_NOIZETIER_LISTER_PAGES_SANS_XML') ? _NOIZETIER_LISTER_PAGES_SANS_XML : true) {
		// 1c- il est autorisé de ne pas avoir de fichier XML de configuration.
		// Ces pages sans XML ne sont chargées qu'une fois, la première. Ensuite, aucune mise à jour n'est nécessaire.
		if (!$options['md5']) {
			$description['icon'] = 'page-xx.svg';
			$md5 = md5('_NOIZETIER_LISTER_PAGES_SANS_XML');
		}
	}

	// Si la description est remplie c'est que le chargement a correctement eu lieu.
	// Sinon, si la page n'a pas changée on renvoie une description limitée à un indicateur d'identité pour
	// distinguer ce cas avec une erreur de chargement qui renvoie une description vide.
	if ($description) {
		// Complétude de la description avec les valeurs par défaut
		$description = array_merge($description_defaut, $description);

		// Traitement du nom de la page en multi avant insertion dans la base. Cela permet ensuite de trier
		// les pages en utilisant ce nom.
		$description['nom'] = compiler_traductions_idiome($description['nom']);
		// Mise à jour du md5
		$description['signature'] = $md5;
		// Identifie si la page est celle d'un objet SPIP
		include_spip('base/objets');
		$tables_objets = array_keys(lister_tables_objets_sql());
		$description['est_page_objet'] = in_array(table_objet_sql($description_defaut['type']), $tables_objets) ? 'oui' : 'non';
		// Traitement des necessite pour identifier l'activité de la page
		if ($description['necessite']) {
			foreach ($description['necessite'] as $_plugin_necessite) {
				if (!defined('_DIR_PLUGIN_' . strtoupper($_plugin_necessite))) {
					$description['est_active'] = 'non';
					break;
				}
			}
		}
		// Sérialisation des champs blocs_exclus, necessite et branche qui sont des tableaux
		$description['blocs_exclus'] = serialize($description['blocs_exclus']);
		$description['necessite'] = serialize($description['necessite']);
		$description['branche'] = serialize($description['branche']);
	} elseif ($md5 == $options['md5']) {
		$description['identique'] = true;
	}

	return $description;
}

/**
 * Compiler les traductions d'un idiome représentant le nom d'une page en une chaine multi.
 *
 * @param string $idiome Idiome sous sa forme <:xx:yy:>
 *
 * @return string Chaine multi des traductions issues de l'idiome.
 */
function compiler_traductions_idiome(string $idiome) : string {
	// On initialise le texte final à l'idiome en considérant que c'est le cas général.
	$texte = $idiome;

	if (
		(strpos($idiome, '<:') !== false)
		and preg_match('@<:([a-z0-9_]+):([a-z0-9_]+):>@isS', $idiome, $match)
	) {
		$module = $match[1];
		$item = $match[2];

		$multi = '';
		if ($fichiers_langue = find_all_in_path('lang/', $module . '_.+[.]php$')) {
			include_spip('inc/lang_liste');
			foreach ($fichiers_langue as $_fichier_langue) {
				$nom_fichier = basename($_fichier_langue, '.php');
				$langue = substr($nom_fichier, strlen($module) + 1 - strlen($nom_fichier));
				// Si la langue est reconnue, on traite la liste des items de langue
				if (isset($GLOBALS['codes_langues'][$langue])) {
					$GLOBALS['idx_lang'] = $langue;
					include("{$_fichier_langue}");
					if (isset($GLOBALS[$langue][$item])) {
						$multi .= ($multi ? "\n" : '') . "[{$langue}]" . $GLOBALS[$langue][$item];
					}
				}
			}
		}

		if ($multi) {
			$texte = '<multi>' . $multi . '</multi>';
		}
	}

	return $texte;
}

/**
 * Construit le tableau des conditions au format compatible avec l'API SQL à partir du tableau des filtres
 * au format [champ] = valeur.
 *
 * @param array $filtres Tableau associatif `[champ] = valeur` de critères de filtres sur les descriptions de types de noisette.
 *                       Les opérateurs binaires possibles sont l'égalité et l'inégalité en préfixant la valeur par le caractère `!``.
 *                       Si la valeur est un tableau on génère un IN.
 *
 * @return array Tableau des conditions SQL issues des filtres
 */
function calculer_condition_par_filtre(array $filtres) : array {
	$where = [];
	if ($filtres) {
		foreach ($filtres as $_champ => $_critere) {
			if (is_array($_critere)) {
				$where[] = sql_in($_champ, $_critere);
			} else {
				$operateur = '=';
				$valeur = $_critere;
				if (substr($_critere, 0, 1) == '!') {
					$operateur = '!=';
					$valeur = ltrim($_critere, '!');
				}
				$where[] = $_champ . $operateur . sql_quote($valeur);
			}
		}
	}

	return $where;
}

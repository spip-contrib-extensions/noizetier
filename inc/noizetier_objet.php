<?php
/**
 * Ce fichier contient l'API de gestion des objets configurables par le noiZetier.
 *
 * @package SPIP\NOIZETIER\OBJET\API
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Renvoie la description complète ou uniquement une information précise pour un objet donné.
 * Cette fonction est utilisable dans le public via la balise #OBJET_NOIZETIER_INFOS.
 *
 * @api
 *
 * @param string      $type_objet  Type de l'objet comme `article`.
 * @param int         $id_objet    Id de l'objet.
 * @param null|string $information Champ précis à renvoyer ou chaîne vide pour renvoyer toutes les champs de l'objet.
 *
 * @return array|int|string La description complète sous forme de tableau ou l'information précise demandée.
 */
function objet_noizetier_lire(string $type_objet, int $id_objet, ?string $information = '') {
	static $description_objet = [];

	if ($type_objet and (int) $id_objet and !isset($description_objet[$type_objet][$id_objet])) {
		include_spip('inc/quete');
		include_spip('base/objets');
		$description = [];

		// On calcule le titre de l'objet à partir de la fonction idoine
		$informer = function_exists('generer_objet_info') ? 'generer_objet_info' : 'generer_info_entite';
		$description['titre'] = $informer($id_objet, $type_objet, 'titre');

		// On recherche le logo de l'objet si il existe sinon on stocke le logo du type d'objet
		// (le chemin complet)
		$description['logo'] = '';
		if ($type_objet != 'document') {
			$logo_infos = quete_logo(id_table_objet($type_objet), 'on', $id_objet, 0, false);
			$description['logo'] = isset($logo_infos['src']) ? $logo_infos['src'] : '';
		}
		if (!$description['logo']) {
			$description['logo'] = chemin_image("{$type_objet}-xx.svg");
		}

		// On récupère le nombre de noisette déjà configurées dans l'objet.
		$description['noisettes'] = 0;
		$from = ['spip_noisettes'];
		$where = [
			'plugin=' . sql_quote('noizetier'),
			'objet=' . sql_quote($type_objet),
			'id_objet=' . (int) $id_objet
		];
		if ($noisettes = sql_countsel($from, $where)) {
			$description['noisettes'] = $noisettes;
		}

		// On rajoute les blocs du type de page dont l'objet est une instance
		include_spip('inc/noizetier_page');
		$description['blocs'] = page_noizetier_lister_blocs($type_objet);

		// On complète avec l'indicateur définissant si les noisettes sont actives sur le type d'objet
		$description['type_est_active'] = objet_noizetier_type_active($type_objet);

		// Et on termine avec les informations sur les compositions:
		// - indicateur de composition activée sur le type de page (donc le type d'objet).
		// - composition active sur l'objet
		$description['composition_est_activee'] = page_noizetier_composition_activee($type_objet);
		$description['composition_active'] = objet_noizetier_lire_composition($type_objet, $id_objet);

		// On sauvegarde finalement la description complète.
		$description_objet[$type_objet][$id_objet] = $description;
	}

	// On retourne les informations sur l'objet demandé.
	if (!$information) {
		$retour = $description_objet[$type_objet][$id_objet] ?? [];
	} else {
		$retour = $description_objet[$type_objet][$id_objet][$information] ?? '';
	}

	return $retour;
}

/**
 * Renvoie la composition associée à un objet.
 *
 * @api
 *
 * @param string $type_objet Le type d'objet comme `article`.
 * @param int    $id_objet   L'id de l'objet.
 *
 * @return string Le nom de la composition seule ou vide sinon.
 */
function objet_noizetier_lire_composition(string $type_objet, int $id_objet) : string {
	$composition = '';

	if (defined('_DIR_PLUGIN_COMPOSITIONS')) {
		include_spip('compositions_fonctions');
		$composition = compositions_determiner($type_objet, $id_objet);
	}

	return $composition;
}

/**
 * Lister les contenus ayant des noisettes spécifiquement configurées pour leur page.
 * Cette fonction est utilisable dans le public via la balise #OBJET_NOIZETIER_LISTE.
 *
 * @api
 *
 * @param null|array $filtres Liste des champs sur lesquels appliquer les filtres des objets.
 *
 * @return array Tableau des descriptions de chaque objet trouvés. Ce tableau est éventuellement filtré sur
 *               un ou plusieurs champs de la description.
 */
function objet_noizetier_repertorier(?array $filtres = []) : array {
	static $objets = null;

	if (null === $objets) {
		// On récupère le ou les objets ayant des noisettes dans la table spip_noisettes.
		$from = ['spip_noisettes'];
		$select = ['objet', 'id_objet', "count(type_noisette) as 'noisettes'"];
		$where = [
			'plugin=' . sql_quote('noizetier'),
			'id_objet>0'
		];
		$group = ['objet', 'id_objet'];
		$objets_configures = sql_allfetsel($select, $from, $where, $group);
		if ($objets_configures) {
			foreach ($objets_configures as $_objet) {
				// On ne retient que les objets dont le type est activé dans la configuration du plugin.
				if (objet_noizetier_type_active($_objet['objet'])) {
					$description = objet_noizetier_lire($_objet['objet'], $_objet['id_objet']);
					if ($description) {
						// Si un filtre existe on teste le contenu de l'objet récupéré avant de le garder
						// sinon on le sauvegarde immédiatement.
						$objet_a_retenir = true;
						if ($filtres) {
							foreach ($filtres as $_critere => $_valeur) {
								if (isset($description[$_critere]) and ($description[$_critere] == $_valeur)) {
									$objet_a_retenir = false;
									break;
								}
							}
						}
						if ($objet_a_retenir) {
							$objets[$_objet['objet']][$_objet['id_objet']] = $description;
						}
					}
				}
			}
		}
	}

	return $objets;
}

/**
 * Détermine si un type d'objet est activé dans la configuration du noiZetier.
 * Si oui, ses objets peuvent recevoir une configuration de noisettes.
 *
 * @api
 *
 * @param string $type_objet Type d'objet SPIP comme article, rubrique...
 *
 * @return bool `truet si le type d'objet est activé, `false` sinon.
 */
function objet_noizetier_type_active(string $type_objet) : bool {
	static $tables_actives = null;
	$est_active = false;

	// Si la liste des tables d'objet actives est null on la calcule une seule fois
	if ($tables_actives === null) {
		include_spip('inc/config');
		$tables_actives = array_map('objet_type', lire_config('noizetier/objets_noisettes', []));
	}

	// Si la liste est non vide, on détermine si le type d'objet est bien activé.
	if ($tables_actives and in_array($type_objet, $tables_actives)) {
		$est_active = true;
	}

	return $est_active;
}

/**
 * Détermine, pour un objet donné, la liste des blocs ayant des noisettes incluses et renvoie leur nombre.
 *
 * @api
 *
 * @param string $type_objet Le type d'objet comme `article`.
 * @param int    $id_objet   L'id de l'objet.
 *
 * @return array Tableau des nombre de noisettes incluses par bloc de la forme [bloc] = nombre de noisettes.
 */
function objet_noizetier_compter_noisettes(string $type_objet, int $id_objet) : array {
	static $blocs_compteur = [];

	if (!isset($blocs_compteur["{$type_objet}-{$id_objet}"])) {
		// Initialisation des compteurs par bloc
		$nb_noisettes = [];

		// Le nombre de noisettes par bloc doit être calculé par une lecture de la table spip_noisettes.
		$from = ['spip_noisettes'];
		$select = ['bloc', "count(type_noisette) as 'noisettes'"];
		// -- Construction du where identifiant précisément le type et la composition de la page
		$where = [
			'plugin=' . sql_quote('noizetier'),
			'objet=' . sql_quote($type_objet),
			'id_objet=' . (int) $id_objet
		];
		$group = ['bloc'];
		$blocs_non_vides = sql_allfetsel($select, $from, $where, $group);
		if ($blocs_non_vides) {
			// On formate le tableau [bloc] = nb noisettes
			$nb_noisettes = array_column($blocs_non_vides, 'noisettes', 'bloc');
		}

		// Sauvegarde des compteurs pour les blocs concernés.
		$blocs_compteur["{$type_objet}-{$id_objet}"] = $nb_noisettes;
	}

	return $blocs_compteur["{$type_objet}-{$id_objet}"];
}

/**
 * Affiche, pour un objet donné, des liens pour configurer les noisettes.
 *
 * @api
 *
 * @param string $type_objet Le type d'objet comme `article`.
 * @param int    $id_objet   L'id de l'objet.
 *
 * @return string Texte HTML des liens.
 */
function objet_noizetier_afficher_configuration(string $type_objet, int $id_objet) : string {
	// Initialiser les caractéristiques standard de l'objet
	include_spip('inc/noizetier_page');
	include_spip('inc/noizetier_objet');
	$contexte = [
		'objet'               => $type_objet,
		'id_objet'            => $id_objet,
		'noisette_activee'    => objet_noizetier_type_active($type_objet),
		'composition_activee' => page_noizetier_composition_activee($type_objet),
	];

	// Déterminer si une composition s'applique à l'objet
	include_spip('compositions_fonctions');
	$contexte['objet_composition'] = defined('_DIR_PLUGIN_COMPOSITIONS')
		? compositions_determiner($type_objet, $id_objet)
		: '';
	$contexte['objet_page'] = $contexte['objet_composition']
		? $type_objet . '-' . $contexte['objet_composition']
		: $type_objet;

	// Déterminer le nombre de noisettes pour la page et l'objet éventuellement
	$contexte['noisettes_objet'] = array_sum(objet_noizetier_compter_noisettes($type_objet, $id_objet));
	$contexte['noisettes_page'] = array_sum(page_noizetier_compter_noisettes($contexte['objet_page']));

	// Calculer l'inclusion à partir du contexte
	$texte = recuperer_fond('prive/squelettes/inclure/inc-noisettes_objet', $contexte);

	return $texte;
}
